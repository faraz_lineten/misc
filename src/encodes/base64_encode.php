<?php
if (PHP_SAPI != "cli") {
    exit;
}
if (2 === $argc) {
    $file = file_get_contents($argv[1]);
    if (!empty($file)) {
        $newFileName = basename($argv[1]) . ".txt";
        file_put_contents(dirname($argv[1]) . DIRECTORY_SEPARATOR . $newFileName, base64_encode($file));
    } else {
        echo "empty file {$argv[1]}";
    }

} else {
    echo "2nd Argument missing: path to file";
}
?>