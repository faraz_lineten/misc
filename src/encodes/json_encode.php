<?php
$array = [
    "TransportType" => "BIKE,CAR,CARGOBIKE,MOTORBIKE,VAN,WALKER",
    "PickupTime" => "2019-02-02 10:00",
    "ClientRef" => "21232f297a57a5a743894a0e4a801fc3",
    "Comments" => "",
    "PickupNotes" => "",
    "DeliveryNotes" => "",
    "DeliveryAddresses" => [
        [
            "Address" =>
                [
                    "Postcode" => "KT5",
                    "Address1" => "KT5"
                ],
            "Person" =>
                [
                    "FirstName" => "admin",
                    "LastName" => "admin",
                    "Phone" => "07000000000",
                ],
        ],
    ],
    "PickupAddress" =>
        [
            "Address" =>
                [
                    "Postcode" => "KT2",
                    "Address1" => "KT2",
                ],
            "Person" =>
                [
                    "FirstName" => "admin",
                    "LastName" => "admin",
                    "Phone" => "7000000000",
                ]
        ],
    "DoNotQuote" => 1,
];

echo json_encode($array);
?>