<?php
$eof = <<<EOF
{
  "status": "ok",
  "error": false,
  "error_messages": [],
  "row_count": 15,
  "key": "",
  "data": [
    {
      "AppendResponse": [
        {
          "Key": "NDSJobId",
          "Value": "17626"
        },
        {
          "Key": "NDSTaskId",
          "Value": "35252"
        }
      ],
      "AuthEndpoint": "",
      "AuthIdentifier": "",
      "CallbackURL": "https://nova-callback-SHOPPERS.onehour-delivery.ca/v2/callback/onfleet",
      "Exec": {
        "Created": "2021-01-29T19:17:03.124807359Z",
        "CronSpec": "@every 60s",
        "ExecCount": 15610,
        "ExecError": 0,
        "Name": "Onfleet GPS Updates",
        "UUID": "91716cab-6266-11eb-a0fc-0210cd05f6ce"
      },
      "Headers": [
        {
          "Key": "Authorization",
          "Value": "Basic YjYzNTM3OGU1OGFjYTVkNjZmNWYxZDU5M2VhNjdmMmY6"
        }
      ],
      "Method": "GET",
      "Payload": "",
      "URL": "https://onfleet.com/api/v2/workers/cAYr*bzlOFOGSyOfl9bbIBYd?analytics=false"
    },
    {
      "AppendResponse": [
        {
          "Key": "NDSJobId",
          "Value": "17847"
        },
        {
          "Key": "NDSTaskId",
          "Value": "35695"
        }
      ],
      "AuthEndpoint": "",
      "AuthIdentifier": "",
      "CallbackURL": "https://nova-callback-SHOPPERS.onehour-delivery.ca/v2/callback/onfleet",
      "Exec": {
        "Created": "2021-02-03T19:03:08.602428899Z",
        "CronSpec": "@every 60s",
        "ExecCount": 8423,
        "ExecError": 0,
        "Name": "Onfleet GPS Updates",
        "UUID": "74182a44-6652-11eb-a0fc-0210cd05f6ce"
      },
      "Headers": [
        {
          "Key": "Authorization",
          "Value": "Basic YjYzNTM3OGU1OGFjYTVkNjZmNWYxZDU5M2VhNjdmMmY6"
        }
      ],
      "Method": "GET",
      "Payload": "",
      "URL": "https://onfleet.com/api/v2/workers/hsNOavl4FZ6qiay5ZmnRRckR?analytics=false"
    },
    {
      "AppendResponse": [
        {
          "Key": "NDSJobId",
          "Value": "17829"
        },
        {
          "Key": "NDSTaskId",
          "Value": "35658"
        }
      ],
      "AuthEndpoint": "",
      "AuthIdentifier": "",
      "CallbackURL": "https://nova-callback-SHOPPERS.onehour-delivery.ca/v2/callback/onfleet",
      "Exec": {
        "Created": "2021-02-03T21:06:33.403622957Z",
        "CronSpec": "@every 60s",
        "ExecCount": 1,
        "ExecError": 0,
        "Name": "Onfleet GPS Updates",
        "UUID": "b1b328ea-6663-11eb-a0fc-0210cd05f6ce"
      },
      "Headers": [
        {
          "Key": "Authorization",
          "Value": "Basic YjYzNTM3OGU1OGFjYTVkNjZmNWYxZDU5M2VhNjdmMmY6"
        }
      ],
      "Method": "GET",
      "Payload": "",
      "URL": "https://onfleet.com/api/v2/workers/N2UDUFsisSv8HkkJuMBgftRQ?analytics=false"
    },
    {
      "AppendResponse": [
        {
          "Key": "NDSJobId",
          "Value": "16611"
        },
        {
          "Key": "NDSTaskId",
          "Value": "33224"
        }
      ],
      "AuthEndpoint": "",
      "AuthIdentifier": "",
      "CallbackURL": "https://nova-callback-SHOPPERS.onehour-delivery.ca/v2/callback/onfleet",
      "Exec": {
        "Created": "2021-01-11T22:22:06.905897838Z",
        "CronSpec": "@every 60s",
        "ExecCount": 41344,
        "ExecError": 2,
        "Name": "Onfleet GPS Updates",
        "UUID": "70607587-545b-11eb-a0fc-0210cd05f6ce"
      },
      "Headers": [
        {
          "Key": "Authorization",
          "Value": "Basic YjYzNTM3OGU1OGFjYTVkNjZmNWYxZDU5M2VhNjdmMmY6"
        }
      ],
      "Method": "GET",
      "Payload": "",
      "URL": "https://onfleet.com/api/v2/workers/JhH0aKeZNV0Pc*OcKEaO~lea?analytics=false"
    },
    {
      "AppendResponse": [
        {
          "Key": "NDSJobId",
          "Value": "15496"
        },
        {
          "Key": "NDSTaskId",
          "Value": "30993"
        }
      ],
      "AuthEndpoint": "",
      "AuthIdentifier": "",
      "CallbackURL": "https://nova-callback-SHOPPERS.onehour-delivery.ca/v2/callback/onfleet",
      "Exec": {
        "Created": "2020-12-16T21:02:02.600471649Z",
        "CronSpec": "@every 60s",
        "ExecCount": 1,
        "ExecError": 0,
        "Name": "Onfleet GPS Updates",
        "UUID": "f20c20d1-3fe1-11eb-926b-0210cd05f6ce"
      },
      "Headers": [
        {
          "Key": "Authorization",
          "Value": "Basic YjYzNTM3OGU1OGFjYTVkNjZmNWYxZDU5M2VhNjdmMmY6"
        }
      ],
      "Method": "GET",
      "Payload": "",
      "URL": "https://onfleet.com/api/v2/workers/N2UDUFsisSv8HkkJuMBgftRQ?analytics=false"
    },
    {
      "AppendResponse": [
        {
          "Key": "NDSJobId",
          "Value": "17498"
        },
        {
          "Key": "NDSTaskId",
          "Value": "34998"
        }
      ],
      "AuthEndpoint": "",
      "AuthIdentifier": "",
      "CallbackURL": "https://nova-callback-SHOPPERS.onehour-delivery.ca/v2/callback/onfleet",
      "Exec": {
        "Created": "2021-01-28T17:57:35.069934991Z",
        "CronSpec": "@every 60s",
        "ExecCount": 1,
        "ExecError": 0,
        "Name": "Onfleet GPS Updates",
        "UUID": "4d0c3d3b-6192-11eb-a0fc-0210cd05f6ce"
      },
      "Headers": [
        {
          "Key": "Authorization",
          "Value": "Basic YTRhN2E0ZDgwNzAzMDBiZjNiYWEwNDkxZWQ2MGI1YjI6"
        }
      ],
      "Method": "GET",
      "Payload": "",
      "URL": "https://onfleet.com/api/v2/workers/Y87Y0wVwgO8lAU~itKmLIv7h?analytics=false"
    },
    {
      "AppendResponse": [
        {
          "Key": "NDSJobId",
          "Value": "18165"
        },
        {
          "Key": "NDSTaskId",
          "Value": "36331"
        }
      ],
      "AuthEndpoint": "",
      "AuthIdentifier": "",
      "CallbackURL": "https://nova-callback-SHOPPERS.onehour-delivery.ca/v2/callback/onfleet",
      "Exec": {
        "Created": "2021-02-09T04:43:03.555444426Z",
        "CronSpec": "@every 60s",
        "ExecCount": 644,
        "ExecError": 0,
        "Name": "Onfleet GPS Updates",
        "UUID": "4b91666e-6a91-11eb-a0fc-0210cd05f6ce"
      },
      "Headers": [
        {
          "Key": "Authorization",
          "Value": "Basic YjYzNTM3OGU1OGFjYTVkNjZmNWYxZDU5M2VhNjdmMmY6"
        }
      ],
      "Method": "GET",
      "Payload": "",
      "URL": "https://onfleet.com/api/v2/workers/j~D*gsPAMcfGub0~d4EjAjr5?analytics=false"
    },
    {
      "AppendResponse": [
        {
          "Key": "NDSJobId",
          "Value": "17989"
        },
        {
          "Key": "NDSTaskId",
          "Value": "35979"
        }
      ],
      "AuthEndpoint": "",
      "AuthIdentifier": "",
      "CallbackURL": "https://nova-callback-SHOPPERS.onehour-delivery.ca/v2/callback/onfleet",
      "Exec": {
        "Created": "2021-02-05T23:10:39.907346197Z",
        "CronSpec": "@every 60s",
        "ExecCount": 5296,
        "ExecError": 0,
        "Name": "Onfleet GPS Updates",
        "UUID": "5cfcf499-6807-11eb-a0fc-0210cd05f6ce"
      },
      "Headers": [
        {
          "Key": "Authorization",
          "Value": "Basic YjYzNTM3OGU1OGFjYTVkNjZmNWYxZDU5M2VhNjdmMmY6"
        }
      ],
      "Method": "GET",
      "Payload": "",
      "URL": "https://onfleet.com/api/v2/workers/hsNOavl4FZ6qiay5ZmnRRckR?analytics=false"
    },
    {
      "AppendResponse": [
        {
          "Key": "NDSJobId",
          "Value": "17379"
        },
        {
          "Key": "NDSTaskId",
          "Value": "34759"
        }
      ],
      "AuthEndpoint": "",
      "AuthIdentifier": "",
      "CallbackURL": "https://nova-callback-SHOPPERS.onehour-delivery.ca/v2/callback/onfleet",
      "Exec": {
        "Created": "2021-01-26T19:56:57.26216907Z",
        "CronSpec": "@every 60s",
        "ExecCount": 19890,
        "ExecError": 2,
        "Name": "Onfleet GPS Updates",
        "UUID": "a5388b5f-6010-11eb-a0fc-0210cd05f6ce"
      },
      "Headers": [
        {
          "Key": "Authorization",
          "Value": "Basic YjYzNTM3OGU1OGFjYTVkNjZmNWYxZDU5M2VhNjdmMmY6"
        }
      ],
      "Method": "GET",
      "Payload": "",
      "URL": "https://onfleet.com/api/v2/workers/HGwgYgbOXNyjbZo~kF*O9vTz?analytics=false"
    },
    {
      "AppendResponse": [
        {
          "Key": "NDSJobId",
          "Value": "17165"
        },
        {
          "Key": "NDSTaskId",
          "Value": "34331"
        }
      ],
      "AuthEndpoint": "",
      "AuthIdentifier": "",
      "CallbackURL": "https://nova-callback-SHOPPERS.onehour-delivery.ca/v2/callback/onfleet",
      "Exec": {
        "Created": "2021-01-21T01:28:27.462384369Z",
        "CronSpec": "@every 60s",
        "ExecCount": 28198,
        "ExecError": 0,
        "Name": "Onfleet GPS Updates",
        "UUID": "f639bd49-5b87-11eb-a0fc-0210cd05f6ce"
      },
      "Headers": [
        {
          "Key": "Authorization",
          "Value": "Basic YjYzNTM3OGU1OGFjYTVkNjZmNWYxZDU5M2VhNjdmMmY6"
        }
      ],
      "Method": "GET",
      "Payload": "",
      "URL": "https://onfleet.com/api/v2/workers/JhH0aKeZNV0Pc*OcKEaO~lea?analytics=false"
    },
    {
      "AppendResponse": [
        {
          "Key": "NDSJobId",
          "Value": "17727"
        },
        {
          "Key": "NDSTaskId",
          "Value": "35455"
        }
      ],
      "AuthEndpoint": "",
      "AuthIdentifier": "",
      "CallbackURL": "https://nova-callback-SHOPPERS.onehour-delivery.ca/v2/callback/onfleet",
      "Exec": {
        "Created": "2021-02-02T23:17:05.627693677Z",
        "CronSpec": "@every 60s",
        "ExecCount": 9610,
        "ExecError": 0,
        "Name": "Onfleet GPS Updates",
        "UUID": "c3a7f5ad-65ac-11eb-a0fc-0210cd05f6ce"
      },
      "Headers": [
        {
          "Key": "Authorization",
          "Value": "Basic YjYzNTM3OGU1OGFjYTVkNjZmNWYxZDU5M2VhNjdmMmY6"
        }
      ],
      "Method": "GET",
      "Payload": "",
      "URL": "https://onfleet.com/api/v2/workers/HGwgYgbOXNyjbZo~kF*O9vTz?analytics=false"
    },
    {
      "AppendResponse": [
        {
          "Key": "NDSJobId",
          "Value": "17466"
        },
        {
          "Key": "NDSTaskId",
          "Value": "34934"
        }
      ],
      "AuthEndpoint": "",
      "AuthIdentifier": "",
      "CallbackURL": "https://nova-callback-SHOPPERS.onehour-delivery.ca/v2/callback/onfleet",
      "Exec": {
        "Created": "2021-01-27T20:26:01.866987192Z",
        "CronSpec": "@every 60s",
        "ExecCount": 18421,
        "ExecError": 1,
        "Name": "Onfleet GPS Updates",
        "UUID": "df7fdd0d-60dd-11eb-a0fc-0210cd05f6ce"
      },
      "Headers": [
        {
          "Key": "Authorization",
          "Value": "Basic YjYzNTM3OGU1OGFjYTVkNjZmNWYxZDU5M2VhNjdmMmY6"
        }
      ],
      "Method": "GET",
      "Payload": "",
      "URL": "https://onfleet.com/api/v2/workers/AzKe5sM5HLHjQ*O~ledVmHtR?analytics=false"
    },
    {
      "AppendResponse": [
        {
          "Key": "NDSJobId",
          "Value": "16819"
        },
        {
          "Key": "NDSTaskId",
          "Value": "33640"
        }
      ],
      "AuthEndpoint": "",
      "AuthIdentifier": "",
      "CallbackURL": "https://nova-callback-SHOPPERS.onehour-delivery.ca/v2/callback/onfleet",
      "Exec": {
        "Created": "2021-01-14T20:48:27.008482555Z",
        "CronSpec": "@every 60s",
        "ExecCount": 37118,
        "ExecError": 3,
        "Name": "Onfleet GPS Updates",
        "UUID": "d9e56abb-56a9-11eb-a0fc-0210cd05f6ce"
      },
      "Headers": [
        {
          "Key": "Authorization",
          "Value": "Basic YjYzNTM3OGU1OGFjYTVkNjZmNWYxZDU5M2VhNjdmMmY6"
        }
      ],
      "Method": "GET",
      "Payload": "",
      "URL": "https://onfleet.com/api/v2/workers/tmWBTAyZU627NY5cyIXC8ckG?analytics=false"
    },
    {
      "AppendResponse": [
        {
          "Key": "NDSJobId",
          "Value": "17347"
        },
        {
          "Key": "NDSTaskId",
          "Value": "34696"
        }
      ],
      "AuthEndpoint": "",
      "AuthIdentifier": "",
      "CallbackURL": "https://nova-callback-SHOPPERS.onehour-delivery.ca/v2/callback/onfleet",
      "Exec": {
        "Created": "2021-01-26T22:08:53.503381844Z",
        "CronSpec": "@every 60s",
        "ExecCount": 19758,
        "ExecError": 2,
        "Name": "Onfleet GPS Updates",
        "UUID": "13ab17bf-6023-11eb-a0fc-0210cd05f6ce"
      },
      "Headers": [
        {
          "Key": "Authorization",
          "Value": "Basic YjYzNTM3OGU1OGFjYTVkNjZmNWYxZDU5M2VhNjdmMmY6"
        }
      ],
      "Method": "GET",
      "Payload": "",
      "URL": "https://onfleet.com/api/v2/workers/HGwgYgbOXNyjbZo~kF*O9vTz?analytics=false"
    },
    {
      "AppendResponse": [
        {
          "Key": "NDSJobId",
          "Value": "17102"
        },
        {
          "Key": "NDSTaskId",
          "Value": "34206"
        }
      ],
      "AuthEndpoint": "",
      "AuthIdentifier": "",
      "CallbackURL": "https://nova-callback-SHOPPERS.onehour-delivery.ca/v2/callback/onfleet",
      "Exec": {
        "Created": "2021-01-21T01:02:56.465234079Z",
        "CronSpec": "@every 60s",
        "ExecCount": 28224,
        "ExecError": 1,
        "Name": "Onfleet GPS Updates",
        "UUID": "65ae1d1c-5b84-11eb-a0fc-0210cd05f6ce"
      },
      "Headers": [
        {
          "Key": "Authorization",
          "Value": "Basic YjYzNTM3OGU1OGFjYTVkNjZmNWYxZDU5M2VhNjdmMmY6"
        }
      ],
      "Method": "GET",
      "Payload": "",
      "URL": "https://onfleet.com/api/v2/workers/hsNOavl4FZ6qiay5ZmnRRckR?analytics=false"
    }
  ]
}
EOF;

$dataObj = json_decode($eof);
$data = $dataObj->data;
$today = new \DateTime('2021-02-11T23:38:08');
$count = 1;
foreach ($data as $aData) {
    $datetime = substr($aData->Exec->Created, 0, strrpos($aData->Exec->Created, '.'));
    $created = new \DateTime($datetime);
    var_dump($datetime, $today > $created);
//    die;
    if (!empty($aData->Exec->UUID) && $today > $created) {

        // $requestUrl = "https://nova-event-test.noqu.cloud/v2/poller/{$aData->Exec->UUID}";
        // $requestUrl = "https://nova-event-shoppers.onehour-delivery.ca/v2/poller/{$aData->Exec->UUID}";
//        $requestUrl = "https://nova-event-staging.noqu.cloud/v2/poller/{$aData->Exec->UUID}";
        $requestUrl = "https://nova-event-shoppers.onehour-delivery.ca/v2/poller/{$aData->Exec->UUID}";
        $result = curl_del($requestUrl);
        echo $created->format('Y:m:d H:i:s');
        echo $requestUrl . PHP_EOL;
        echo print_r($result);
        echo "count: " . $count. PHP_EOL;
        echo PHP_EOL . PHP_EOL . PHP_EOL;
        $count++;

    }
}

function curl_del($path)
{
    $url = $path;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
    $result = curl_exec($ch);
    $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);

    return $result;
}