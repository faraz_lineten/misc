<?php
$dbConnection = new PDO('mysql:host=db-local.noqu.delivery:3306', 'root', 'admin');
foreach( $dbConnection->query("show databases") as $schema){
    $schemaName = $schema['Database'];
   if(false !== strpos($schemaName, '_phpunit_')){
       echo $schemaName. PHP_EOL;
       $dbConnection->query("DROP DATABASE IF EXISTS {$schemaName}");
   }
}