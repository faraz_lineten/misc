<?php

class docTypeRef_TotalNetType
{

  /**
   * 
   * @var string $Currency
   * @access public
   */
  public $Currency = null;

  /**
   * 
   * @var string $Amount
   * @access public
   */
  public $Amount = null;

  /**
   * 
   * @param string $Currency
   * @param string $Amount
   * @access public
   */
  public function __construct($Currency, $Amount)
  {
    $this->Currency = $Currency;
    $this->Amount = $Amount;
  }

}
