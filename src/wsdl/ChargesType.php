<?php

class ChargesType
{

  /**
   * 
   * @var ChargeType $ChargeType
   * @access public
   */
  public $ChargeType = null;

  /**
   * 
   * @var Money2 $ChargeAmount
   * @access public
   */
  public $ChargeAmount = null;

  /**
   * 
   * @var CurrencyCode2 $CurrencyCode
   * @access public
   */
  public $CurrencyCode = null;

  /**
   * 
   * @param ChargeType $ChargeType
   * @param Money2 $ChargeAmount
   * @param CurrencyCode2 $CurrencyCode
   * @access public
   */
  public function __construct($ChargeType, $ChargeAmount, $CurrencyCode)
  {
    $this->ChargeType = $ChargeType;
    $this->ChargeAmount = $ChargeAmount;
    $this->CurrencyCode = $CurrencyCode;
  }

}
