<?php

class docTypeRef_AddressType2
{

  /**
   * 
   * @var StreetLines4 $StreetLines
   * @access public
   */
  public $StreetLines = null;

  /**
   * 
   * @var StreetLines22 $StreetLines2
   * @access public
   */
  public $StreetLines2 = null;

  /**
   * 
   * @var StreetLines32 $StreetLines3
   * @access public
   */
  public $StreetLines3 = null;

  /**
   * 
   * @var StreetName2 $StreetName
   * @access public
   */
  public $StreetName = null;

  /**
   * 
   * @var StreetNumber2 $StreetNumber
   * @access public
   */
  public $StreetNumber = null;

  /**
   * 
   * @var City2 $City
   * @access public
   */
  public $City = null;

  /**
   * 
   * @var CityDistrict2 $CityDistrict
   * @access public
   */
  public $CityDistrict = null;

  /**
   * 
   * @var string $StateOrProvinceCode
   * @access public
   */
  public $StateOrProvinceCode = null;

  /**
   * 
   * @var PostalCode2 $PostalCode
   * @access public
   */
  public $PostalCode = null;

  /**
   * 
   * @var CountryCode $CountryCode
   * @access public
   */
  public $CountryCode = null;

  /**
   * 
   * @var docTypeRef_ContactType $Contact
   * @access public
   */
  public $Contact = null;

  /**
   * 
   * @param StreetLines4 $StreetLines
   * @param StreetLines22 $StreetLines2
   * @param StreetLines32 $StreetLines3
   * @param StreetName2 $StreetName
   * @param StreetNumber2 $StreetNumber
   * @param City2 $City
   * @param CityDistrict2 $CityDistrict
   * @param string $StateOrProvinceCode
   * @param PostalCode2 $PostalCode
   * @param CountryCode $CountryCode
   * @param docTypeRef_ContactType $Contact
   * @access public
   */
  public function __construct($StreetLines, $StreetLines2, $StreetLines3, $StreetName, $StreetNumber, $City, $CityDistrict, $StateOrProvinceCode, $PostalCode, $CountryCode, $Contact)
  {
    $this->StreetLines = $StreetLines;
    $this->StreetLines2 = $StreetLines2;
    $this->StreetLines3 = $StreetLines3;
    $this->StreetName = $StreetName;
    $this->StreetNumber = $StreetNumber;
    $this->City = $City;
    $this->CityDistrict = $CityDistrict;
    $this->StateOrProvinceCode = $StateOrProvinceCode;
    $this->PostalCode = $PostalCode;
    $this->CountryCode = $CountryCode;
    $this->Contact = $Contact;
  }

}
