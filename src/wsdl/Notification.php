<?php

class Notification
{

  /**
   * 
   * @var Message $Message
   * @access public
   */
  public $Message = null;

  /**
   * 
   * @var DetailedMessage $DetailedMessage
   * @access public
   */
  public $DetailedMessage = null;

  /**
   * 
   * @var string $Code
   * @access public
   */
  public $Code = null;

  /**
   * 
   * @param Message $Message
   * @param DetailedMessage $DetailedMessage
   * @param string $Code
   * @access public
   */
  public function __construct($Message, $DetailedMessage, $Code)
  {
    $this->Message = $Message;
    $this->DetailedMessage = $DetailedMessage;
    $this->Code = $Code;
  }

}
