<?php

include_once('docTypeRef_ProcessShipmentRequestType.php');
include_once('docTypeRef_ClientDetailType2.php');
include_once('docTypeRef_Content.php');
include_once('docTypeRef_DangerousGoods.php');
include_once('docTypeRef_RequestedShipmentType.php');
include_once('docTypeRef_ShipmentInfoType.php');
include_once('DropOffType.php');
include_once('UnitOfMeasurement.php');
include_once('PaymentInfo.php');
include_once('docTypeRef_InternationDetailType.php');
include_once('docTypeRef_CommoditiesType.php');
include_once('Content.php');
include_once('docTypeRef_OnDemandDeliveryOptions.php');
include_once('DeliveryOption.php');
include_once('LWNTypeCode.php');
include_once('docTypeRef_ShipType.php');
include_once('docTypeRef_ContactInfoType.php');
include_once('docTypeRef_ContactInfoType1.php');
include_once('docTypeRef_ContactType.php');
include_once('docTypeRef_ContactType1.php');
include_once('docTypeRef_AddressType.php');
include_once('docTypeRef_AddressType1.php');
include_once('docTypeRef_RegistrationNumbers.php');
include_once('docTypeRef_RegistrationNumber.php');
include_once('docTypeRef_PackagesType.php');
include_once('docTypeRef_RequestedPackagesType.php');
include_once('docTypeRef_DimensionsType.php');
include_once('Billing.php');
include_once('ShipmentPaymentType.php');
include_once('Services.php');
include_once('Service.php');
include_once('LabelType.php');
include_once('docTypeRef_RateRequestType.php');
include_once('docTypeRef_ClientDetailType3.php');
include_once('RequestType.php');
include_once('ServiceHeaderType.php');
include_once('docTypeRef_RequestedShipmentType2.php');
include_once('DropOffType2.php');
include_once('NextBusinessDay2.php');
include_once('docTypeRef_ShipType2.php');
include_once('docTypeRef_AddressType2.php');
include_once('docTypeRef_PackagesType2.php');
include_once('docTypeRef_RequestedPackagesType2.php');
include_once('docTypeRef_WeightType.php');
include_once('docTypeRef_DimensionsType2.php');
include_once('UnitOfMeasurement2.php');
include_once('Content2.php');
include_once('PaymentInfo2.php');
include_once('Billing2.php');
include_once('ShipmentPaymentType2.php');
include_once('Services2.php');
include_once('Service2.php');
include_once('LandedCostType.php');
include_once('DutyTaxPaid2.php');
include_once('GetItemCostBreakdown2.php');
include_once('ShipmentPurpose2.php');
include_once('ShipmentTransportationMode2.php');
include_once('MerchantSelectedCarrierName2.php');
include_once('ItemsType.php');
include_once('ItemType.php');
include_once('GoodsCharacteristicsType.php');
include_once('GoodsCharacteristicType.php');
include_once('ShipmentMonetaryAmountType.php');
include_once('ChargesType.php');
include_once('docTypeRef_DeleteResponseType.php');
include_once('docTypeRef_NotificationType.php');
include_once('docTypeRef_DeleteRequestType.php');
include_once('docTypeRef_ClientDetailType.php');
include_once('docTypeRef_RateResponseType.php');
include_once('docTypeRef_ProviderType.php');
include_once('docTypeRef_NotificationType3.php');
include_once('docTypeRef_ServiceType.php');
include_once('_x0040_account.php');
include_once('docTypeRef_TotalNetType.php');
include_once('docTypeRef_ChargesType.php');
include_once('docTypeRef_ChargeType.php');
include_once('docTypeRef_ItemsType.php');
include_once('docTypeRef_ItemType.php');
include_once('Notification.php');
include_once('NextBusinessDayInd2.php');
include_once('docTypeRef_ShipmentDetailType.php');
include_once('docTypeRef_NotificationType2.php');
include_once('docTypeRef_PackagesResultsType.php');
include_once('docTypeRef_PackageResultType.php');
include_once('docTypeRef_LabelImageType.php');


/**
 * 
 */
class GblExpressRateBook extends \SoapClient
{

  /**
   * 
   * @var array $classmap The defined classes
   * @access private
   */
  private static $classmap = array(
    'docTypeRef_ProcessShipmentRequestType' => '\docTypeRef_ProcessShipmentRequestType',
    'docTypeRef_ClientDetailType2' => '\docTypeRef_ClientDetailType2',
    'docTypeRef_Content' => '\docTypeRef_Content',
    'docTypeRef_DangerousGoods' => '\docTypeRef_DangerousGoods',
    'docTypeRef_RequestedShipmentType' => '\docTypeRef_RequestedShipmentType',
    'docTypeRef_ShipmentInfoType' => '\docTypeRef_ShipmentInfoType',
    'docTypeRef_InternationDetailType' => '\docTypeRef_InternationDetailType',
    'docTypeRef_CommoditiesType' => '\docTypeRef_CommoditiesType',
    'docTypeRef_OnDemandDeliveryOptions' => '\docTypeRef_OnDemandDeliveryOptions',
    'docTypeRef_ShipType' => '\docTypeRef_ShipType',
    'docTypeRef_ContactInfoType' => '\docTypeRef_ContactInfoType',
    'docTypeRef_ContactInfoType1' => '\docTypeRef_ContactInfoType1',
    'docTypeRef_ContactType' => '\docTypeRef_ContactType',
    'docTypeRef_ContactType1' => '\docTypeRef_ContactType1',
    'docTypeRef_AddressType' => '\docTypeRef_AddressType',
    'docTypeRef_AddressType1' => '\docTypeRef_AddressType1',
    'docTypeRef_RegistrationNumbers' => '\docTypeRef_RegistrationNumbers',
    'docTypeRef_RegistrationNumber' => '\docTypeRef_RegistrationNumber',
    'docTypeRef_PackagesType' => '\docTypeRef_PackagesType',
    'docTypeRef_RequestedPackagesType' => '\docTypeRef_RequestedPackagesType',
    'docTypeRef_DimensionsType' => '\docTypeRef_DimensionsType',
    'Billing' => '\Billing',
    'Services' => '\Services',
    'Service' => '\Service',
    'docTypeRef_RateRequestType' => '\docTypeRef_RateRequestType',
    'docTypeRef_ClientDetailType3' => '\docTypeRef_ClientDetailType3',
    'RequestType' => '\RequestType',
    'ServiceHeaderType' => '\ServiceHeaderType',
    'docTypeRef_RequestedShipmentType2' => '\docTypeRef_RequestedShipmentType2',
    'docTypeRef_ShipType2' => '\docTypeRef_ShipType2',
    'docTypeRef_AddressType2' => '\docTypeRef_AddressType2',
    'docTypeRef_ContactType' => '\docTypeRef_ContactType',
    'docTypeRef_PackagesType2' => '\docTypeRef_PackagesType2',
    'docTypeRef_RequestedPackagesType2' => '\docTypeRef_RequestedPackagesType2',
    'docTypeRef_WeightType' => '\docTypeRef_WeightType',
    'docTypeRef_DimensionsType2' => '\docTypeRef_DimensionsType2',
    'Billing2' => '\Billing2',
    'Services2' => '\Services2',
    'Service2' => '\Service2',
    'LandedCostType' => '\LandedCostType',
    'ItemsType' => '\ItemsType',
    'ItemType' => '\ItemType',
    'GoodsCharacteristicsType' => '\GoodsCharacteristicsType',
    'GoodsCharacteristicType' => '\GoodsCharacteristicType',
    'ShipmentMonetaryAmountType' => '\ShipmentMonetaryAmountType',
    'ChargesType' => '\ChargesType',
    'docTypeRef_DeleteResponseType' => '\docTypeRef_DeleteResponseType',
    'docTypeRef_NotificationType' => '\docTypeRef_NotificationType',
    'docTypeRef_DeleteRequestType' => '\docTypeRef_DeleteRequestType',
    'docTypeRef_ClientDetailType' => '\docTypeRef_ClientDetailType',
    'docTypeRef_RateResponseType' => '\docTypeRef_RateResponseType',
    'docTypeRef_ProviderType' => '\docTypeRef_ProviderType',
    'ServiceHeaderType' => '\ServiceHeaderType',
    'docTypeRef_NotificationType3' => '\docTypeRef_NotificationType3',
    'docTypeRef_ServiceType' => '\docTypeRef_ServiceType',
    'docTypeRef_TotalNetType' => '\docTypeRef_TotalNetType',
    'docTypeRef_ChargesType' => '\docTypeRef_ChargesType',
    'docTypeRef_ChargeType' => '\docTypeRef_ChargeType',
    'docTypeRef_ItemsType' => '\docTypeRef_ItemsType',
    'docTypeRef_ItemType' => '\docTypeRef_ItemType',
    'Notification' => '\Notification',
    'docTypeRef_ShipmentDetailType' => '\docTypeRef_ShipmentDetailType',
    'docTypeRef_NotificationType2' => '\docTypeRef_NotificationType2',
    'docTypeRef_PackagesResultsType' => '\docTypeRef_PackagesResultsType',
    'docTypeRef_PackageResultType' => '\docTypeRef_PackageResultType',
    'docTypeRef_LabelImageType' => '\docTypeRef_LabelImageType');

  /**
   * 
   * @param array $options A array of config values
   * @param string $wsdl The wsdl file to use
   * @access public
   */
  public function __construct(array $options = array(), $wsdl = 'https://wsbexpress.dhl.com/sndpt/expressRateBook?WSDL')
  {
    foreach (self::$classmap as $key => $value) {
      if (!isset($options['classmap'][$key])) {
        $options['classmap'][$key] = $value;
      }
    }
    
    parent::__construct($wsdl, $options);
  }

  /**
   * 
   * @param docTypeRef_ProcessShipmentRequestType $parameters
   * @access public
   * @return docTypeRef_ShipmentDetailType
   */
  public function createShipmentRequest(docTypeRef_ProcessShipmentRequestType $parameters)
  {
    return $this->__soapCall('createShipmentRequest', array($parameters));
  }

  /**
   * 
   * @param docTypeRef_RateRequestType $parameters
   * @access public
   * @return docTypeRef_RateResponseType
   */
  public function getRateRequest(docTypeRef_RateRequestType $parameters)
  {
    return $this->__soapCall('getRateRequest', array($parameters));
  }

  /**
   * 
   * @param docTypeRef_DeleteRequestType $parameters
   * @access public
   * @return docTypeRef_DeleteResponseType
   */
  public function deleteShipmentRequest(docTypeRef_DeleteRequestType $parameters)
  {
    return $this->__soapCall('deleteShipmentRequest', array($parameters));
  }

}
