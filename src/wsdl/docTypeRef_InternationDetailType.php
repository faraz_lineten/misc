<?php

class docTypeRef_InternationDetailType
{

  /**
   * 
   * @var docTypeRef_CommoditiesType $Commodities
   * @access public
   */
  public $Commodities = null;

  /**
   * 
   * @var Content $Content
   * @access public
   */
  public $Content = null;

  /**
   * 
   * @var ExportReference $ExportReference
   * @access public
   */
  public $ExportReference = null;

  /**
   * 
   * @param docTypeRef_CommoditiesType $Commodities
   * @param Content $Content
   * @param ExportReference $ExportReference
   * @access public
   */
  public function __construct($Commodities, $Content, $ExportReference)
  {
    $this->Commodities = $Commodities;
    $this->Content = $Content;
    $this->ExportReference = $ExportReference;
  }

}
