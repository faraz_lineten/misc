<?php

class docTypeRef_ContactInfoType1
{

  /**
   * 
   * @var docTypeRef_ContactType1 $Contact
   * @access public
   */
  public $Contact = null;

  /**
   * 
   * @var docTypeRef_AddressType1 $Address
   * @access public
   */
  public $Address = null;

  /**
   * 
   * @param docTypeRef_ContactType1 $Contact
   * @param docTypeRef_AddressType1 $Address
   * @access public
   */
  public function __construct($Contact, $Address)
  {
    $this->Contact = $Contact;
    $this->Address = $Address;
  }

}
