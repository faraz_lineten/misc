<?php

class Services2
{

  /**
   * 
   * @var Service2[] $Service
   * @access public
   */
  public $Service = null;

  /**
   * 
   * @param Service2[] $Service
   * @access public
   */
  public function __construct($Service)
  {
    $this->Service = $Service;
  }

}
