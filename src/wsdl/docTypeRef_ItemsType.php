<?php

class docTypeRef_ItemsType
{

  /**
   * 
   * @var docTypeRef_ItemType[] $Item
   * @access public
   */
  public $Item = null;

  /**
   * 
   * @param docTypeRef_ItemType[] $Item
   * @access public
   */
  public function __construct($Item)
  {
    $this->Item = $Item;
  }

}
