<?php

class docTypeRef_ItemType
{

  /**
   * 
   * @var int $ItemNumber
   * @access public
   */
  public $ItemNumber = null;

  /**
   * 
   * @var docTypeRef_ChargeType $Charge
   * @access public
   */
  public $Charge = null;

  /**
   * 
   * @var Notification $Notification
   * @access public
   */
  public $Notification = null;

  /**
   * 
   * @param int $ItemNumber
   * @param docTypeRef_ChargeType $Charge
   * @param Notification $Notification
   * @access public
   */
  public function __construct($ItemNumber, $Charge, $Notification)
  {
    $this->ItemNumber = $ItemNumber;
    $this->Charge = $Charge;
    $this->Notification = $Notification;
  }

}
