<?php

class docTypeRef_LabelImageType
{

  /**
   * 
   * @var LabelImageFormat $LabelImageFormat
   * @access public
   */
  public $LabelImageFormat = null;

  /**
   * 
   * @var base64Binary $GraphicImage
   * @access public
   */
  public $GraphicImage = null;

  /**
   * 
   * @var base64Binary $HTMLImage
   * @access public
   */
  public $HTMLImage = null;

  /**
   * 
   * @param LabelImageFormat $LabelImageFormat
   * @param base64Binary $GraphicImage
   * @param base64Binary $HTMLImage
   * @access public
   */
  public function __construct($LabelImageFormat, $GraphicImage, $HTMLImage)
  {
    $this->LabelImageFormat = $LabelImageFormat;
    $this->GraphicImage = $GraphicImage;
    $this->HTMLImage = $HTMLImage;
  }

}
