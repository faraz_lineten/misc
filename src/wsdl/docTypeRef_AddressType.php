<?php

class docTypeRef_AddressType
{

  /**
   * 
   * @var StreetLines $StreetLines
   * @access public
   */
  public $StreetLines = null;

  /**
   * 
   * @var StreetName $StreetName
   * @access public
   */
  public $StreetName = null;

  /**
   * 
   * @var StreetNumber $StreetNumber
   * @access public
   */
  public $StreetNumber = null;

  /**
   * 
   * @var StreetLines2 $StreetLines2
   * @access public
   */
  public $StreetLines2 = null;

  /**
   * 
   * @var StreetLines3 $StreetLines3
   * @access public
   */
  public $StreetLines3 = null;

  /**
   * 
   * @var City $City
   * @access public
   */
  public $City = null;

  /**
   * 
   * @var StateOrProvinceCode $StateOrProvinceCode
   * @access public
   */
  public $StateOrProvinceCode = null;

  /**
   * 
   * @var PostalCode $PostalCode
   * @access public
   */
  public $PostalCode = null;

  /**
   * 
   * @var CountryCode $CountryCode
   * @access public
   */
  public $CountryCode = null;

  /**
   * 
   * @param StreetLines $StreetLines
   * @param StreetName $StreetName
   * @param StreetNumber $StreetNumber
   * @param StreetLines2 $StreetLines2
   * @param StreetLines3 $StreetLines3
   * @param City $City
   * @param StateOrProvinceCode $StateOrProvinceCode
   * @param PostalCode $PostalCode
   * @param CountryCode $CountryCode
   * @access public
   */
  public function __construct($StreetLines, $StreetName, $StreetNumber, $StreetLines2, $StreetLines3, $City, $StateOrProvinceCode, $PostalCode, $CountryCode)
  {
    $this->StreetLines = $StreetLines;
    $this->StreetName = $StreetName;
    $this->StreetNumber = $StreetNumber;
    $this->StreetLines2 = $StreetLines2;
    $this->StreetLines3 = $StreetLines3;
    $this->City = $City;
    $this->StateOrProvinceCode = $StateOrProvinceCode;
    $this->PostalCode = $PostalCode;
    $this->CountryCode = $CountryCode;
  }

}
