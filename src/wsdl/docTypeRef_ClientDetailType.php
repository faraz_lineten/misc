<?php

class docTypeRef_ClientDetailType
{

  /**
   * 
   * @var sso $sso
   * @access public
   */
  public $sso = null;

  /**
   * 
   * @var plant $plant
   * @access public
   */
  public $plant = null;

  /**
   * 
   * @param sso $sso
   * @param plant $plant
   * @access public
   */
  public function __construct($sso, $plant)
  {
    $this->sso = $sso;
    $this->plant = $plant;
  }

}
