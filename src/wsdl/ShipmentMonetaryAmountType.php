<?php

class ShipmentMonetaryAmountType
{

  /**
   * 
   * @var ChargesType $Charges
   * @access public
   */
  public $Charges = null;

  /**
   * 
   * @param ChargesType $Charges
   * @access public
   */
  public function __construct($Charges)
  {
    $this->Charges = $Charges;
  }

}
