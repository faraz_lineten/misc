<?php

class docTypeRef_DimensionsType
{

  /**
   * 
   * @var Length $Length
   * @access public
   */
  public $Length = null;

  /**
   * 
   * @var Width $Width
   * @access public
   */
  public $Width = null;

  /**
   * 
   * @var Height $Height
   * @access public
   */
  public $Height = null;

  /**
   * 
   * @param Length $Length
   * @param Width $Width
   * @param Height $Height
   * @access public
   */
  public function __construct($Length, $Width, $Height)
  {
    $this->Length = $Length;
    $this->Width = $Width;
    $this->Height = $Height;
  }

}
