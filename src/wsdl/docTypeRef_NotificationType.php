<?php

class docTypeRef_NotificationType
{

  /**
   * 
   * @var Message $Message
   * @access public
   */
  public $Message = null;

  /**
   * 
   * @var _x0040_code $code
   * @access public
   */
  public $code = null;

  /**
   * 
   * @param Message $Message
   * @param _x0040_code $code
   * @access public
   */
  public function __construct($Message, $code)
  {
    $this->Message = $Message;
    $this->code = $code;
  }

}
