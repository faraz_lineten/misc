<?php

class docTypeRef_DeleteRequestType
{

  /**
   * 
   * @var docTypeRef_ClientDetailType $ClientDetail
   * @access public
   */
  public $ClientDetail = null;

  /**
   * 
   * @var date $PickupDate
   * @access public
   */
  public $PickupDate = null;

  /**
   * 
   * @var CountryCode $PickupCountry
   * @access public
   */
  public $PickupCountry = null;

  /**
   * 
   * @var ConfirmationNumberType $DispatchConfirmationNumber
   * @access public
   */
  public $DispatchConfirmationNumber = null;

  /**
   * 
   * @var PersonName3 $RequestorName
   * @access public
   */
  public $RequestorName = null;

  /**
   * 
   * @var CustomerReferences $Reason
   * @access public
   */
  public $Reason = null;

  /**
   * 
   * @param docTypeRef_ClientDetailType $ClientDetail
   * @param date $PickupDate
   * @param CountryCode $PickupCountry
   * @param ConfirmationNumberType $DispatchConfirmationNumber
   * @param PersonName3 $RequestorName
   * @param CustomerReferences $Reason
   * @access public
   */
  public function __construct($ClientDetail, $PickupDate, $PickupCountry, $DispatchConfirmationNumber, $RequestorName, $Reason)
  {
    $this->ClientDetail = $ClientDetail;
    $this->PickupDate = $PickupDate;
    $this->PickupCountry = $PickupCountry;
    $this->DispatchConfirmationNumber = $DispatchConfirmationNumber;
    $this->RequestorName = $RequestorName;
    $this->Reason = $Reason;
  }

}
