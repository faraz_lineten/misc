<?php

class GoodsCharacteristicsType
{

  /**
   * 
   * @var GoodsCharacteristicType $GoodsCharacteristic
   * @access public
   */
  public $GoodsCharacteristic = null;

  /**
   * 
   * @param GoodsCharacteristicType $GoodsCharacteristic
   * @access public
   */
  public function __construct($GoodsCharacteristic)
  {
    $this->GoodsCharacteristic = $GoodsCharacteristic;
  }

}
