<?php

class docTypeRef_ShipmentDetailType
{

  /**
   * 
   * @var docTypeRef_NotificationType2[] $Notification
   * @access public
   */
  public $Notification = null;

  /**
   * 
   * @var docTypeRef_PackagesResultsType $PackagesResult
   * @access public
   */
  public $PackagesResult = null;

  /**
   * 
   * @var docTypeRef_LabelImageType[] $LabelImage
   * @access public
   */
  public $LabelImage = null;

  /**
   * 
   * @var ShipmentIdentificationNumber3 $ShipmentIdentificationNumber
   * @access public
   */
  public $ShipmentIdentificationNumber = null;

  /**
   * 
   * @var string $DispatchConfirmationNumber
   * @access public
   */
  public $DispatchConfirmationNumber = null;

  /**
   * 
   * @param docTypeRef_NotificationType2[] $Notification
   * @param docTypeRef_PackagesResultsType $PackagesResult
   * @param docTypeRef_LabelImageType[] $LabelImage
   * @param ShipmentIdentificationNumber3 $ShipmentIdentificationNumber
   * @param string $DispatchConfirmationNumber
   * @access public
   */
  public function __construct($Notification, $PackagesResult, $LabelImage, $ShipmentIdentificationNumber, $DispatchConfirmationNumber)
  {
    $this->Notification = $Notification;
    $this->PackagesResult = $PackagesResult;
    $this->LabelImage = $LabelImage;
    $this->ShipmentIdentificationNumber = $ShipmentIdentificationNumber;
    $this->DispatchConfirmationNumber = $DispatchConfirmationNumber;
  }

}
