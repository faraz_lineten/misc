<?php

class docTypeRef_ClientDetailType3
{

  /**
   * 
   * @var sso2 $sso
   * @access public
   */
  public $sso = null;

  /**
   * 
   * @var plant2 $plant
   * @access public
   */
  public $plant = null;

  /**
   * 
   * @param sso2 $sso
   * @param plant2 $plant
   * @access public
   */
  public function __construct($sso, $plant)
  {
    $this->sso = $sso;
    $this->plant = $plant;
  }

}
