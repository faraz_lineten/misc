<?php

class docTypeRef_RateRequestType
{

  /**
   * 
   * @var docTypeRef_ClientDetailType3 $ClientDetail
   * @access public
   */
  public $ClientDetail = null;

  /**
   * 
   * @var RequestType $Request
   * @access public
   */
  public $Request = null;

  /**
   * 
   * @var docTypeRef_RequestedShipmentType2 $RequestedShipment
   * @access public
   */
  public $RequestedShipment = null;

  /**
   * 
   * @param docTypeRef_ClientDetailType3 $ClientDetail
   * @param RequestType $Request
   * @param docTypeRef_RequestedShipmentType2 $RequestedShipment
   * @access public
   */
  public function __construct($ClientDetail, $Request, $RequestedShipment)
  {
    $this->ClientDetail = $ClientDetail;
    $this->Request = $Request;
    $this->RequestedShipment = $RequestedShipment;
  }

}
