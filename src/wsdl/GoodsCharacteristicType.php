<?php

class GoodsCharacteristicType
{

  /**
   * 
   * @var CharacteristicCode $CharacteristicCode
   * @access public
   */
  public $CharacteristicCode = null;

  /**
   * 
   * @var CharacteristicValue $CharacteristicValue
   * @access public
   */
  public $CharacteristicValue = null;

  /**
   * 
   * @param CharacteristicCode $CharacteristicCode
   * @param CharacteristicValue $CharacteristicValue
   * @access public
   */
  public function __construct($CharacteristicCode, $CharacteristicValue)
  {
    $this->CharacteristicCode = $CharacteristicCode;
    $this->CharacteristicValue = $CharacteristicValue;
  }

}
