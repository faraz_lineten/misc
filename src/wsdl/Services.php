<?php

class Services
{

  /**
   * 
   * @var Service[] $Service
   * @access public
   */
  public $Service = null;

  /**
   * 
   * @param Service[] $Service
   * @access public
   */
  public function __construct($Service)
  {
    $this->Service = $Service;
  }

}
