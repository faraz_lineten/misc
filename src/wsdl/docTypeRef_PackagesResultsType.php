<?php

class docTypeRef_PackagesResultsType
{

  /**
   * 
   * @var docTypeRef_PackageResultType[] $PackageResult
   * @access public
   */
  public $PackageResult = null;

  /**
   * 
   * @param docTypeRef_PackageResultType[] $PackageResult
   * @access public
   */
  public function __construct($PackageResult)
  {
    $this->PackageResult = $PackageResult;
  }

}
