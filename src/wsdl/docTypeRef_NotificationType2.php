<?php

class docTypeRef_NotificationType2
{

  /**
   * 
   * @var Message $Message
   * @access public
   */
  public $Message = null;

  /**
   * 
   * @var _x0040_code2 $code
   * @access public
   */
  public $code = null;

  /**
   * 
   * @param Message $Message
   * @param _x0040_code2 $code
   * @access public
   */
  public function __construct($Message, $code)
  {
    $this->Message = $Message;
    $this->code = $code;
  }

}
