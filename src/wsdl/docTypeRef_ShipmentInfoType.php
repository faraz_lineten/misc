<?php

class docTypeRef_ShipmentInfoType
{

  /**
   * 
   * @var DropOffType $DropOffType
   * @access public
   */
  public $DropOffType = null;

  /**
   * 
   * @var ServiceType $ServiceType
   * @access public
   */
  public $ServiceType = null;

  /**
   * 
   * @var Account $Account
   * @access public
   */
  public $Account = null;

  /**
   * 
   * @var Billing $Billing
   * @access public
   */
  public $Billing = null;

  /**
   * 
   * @var Services $SpecialServices
   * @access public
   */
  public $SpecialServices = null;

  /**
   * 
   * @var Currency $Currency
   * @access public
   */
  public $Currency = null;

  /**
   * 
   * @var UnitOfMeasurement $UnitOfMeasurement
   * @access public
   */
  public $UnitOfMeasurement = null;

  /**
   * 
   * @var ShipmentIdentificationNumber2 $ShipmentIdentificationNumber
   * @access public
   */
  public $ShipmentIdentificationNumber = null;

  /**
   * 
   * @var UseOwnShipmentIdentificationNumber $UseOwnShipmentIdentificationNumber
   * @access public
   */
  public $UseOwnShipmentIdentificationNumber = null;

  /**
   * 
   * @var PackagesCount $PackagesCount
   * @access public
   */
  public $PackagesCount = null;

  /**
   * 
   * @var SendPackage $SendPackage
   * @access public
   */
  public $SendPackage = null;

  /**
   * 
   * @var LabelType $LabelType
   * @access public
   */
  public $LabelType = null;

  /**
   * 
   * @var LabelTemplate $LabelTemplate
   * @access public
   */
  public $LabelTemplate = null;

  /**
   * 
   * @var ArchiveLabelTemplate $ArchiveLabelTemplate
   * @access public
   */
  public $ArchiveLabelTemplate = null;

  /**
   * 
   * @var boolean $PaperlessTradeEnabled
   * @access public
   */
  public $PaperlessTradeEnabled = null;

  /**
   * 
   * @var base64Binary $PaperlessTradeImage
   * @access public
   */
  public $PaperlessTradeImage = null;

  /**
   * 
   * @param DropOffType $DropOffType
   * @param ServiceType $ServiceType
   * @param Account $Account
   * @param Billing $Billing
   * @param Services $SpecialServices
   * @param Currency $Currency
   * @param UnitOfMeasurement $UnitOfMeasurement
   * @param ShipmentIdentificationNumber2 $ShipmentIdentificationNumber
   * @param UseOwnShipmentIdentificationNumber $UseOwnShipmentIdentificationNumber
   * @param PackagesCount $PackagesCount
   * @param SendPackage $SendPackage
   * @param LabelType $LabelType
   * @param LabelTemplate $LabelTemplate
   * @param ArchiveLabelTemplate $ArchiveLabelTemplate
   * @param boolean $PaperlessTradeEnabled
   * @param base64Binary $PaperlessTradeImage
   * @access public
   */
  public function __construct($DropOffType, $ServiceType, $Account, $Billing, $SpecialServices, $Currency, $UnitOfMeasurement, $ShipmentIdentificationNumber, $UseOwnShipmentIdentificationNumber, $PackagesCount, $SendPackage, $LabelType, $LabelTemplate, $ArchiveLabelTemplate, $PaperlessTradeEnabled, $PaperlessTradeImage)
  {
    $this->DropOffType = $DropOffType;
    $this->ServiceType = $ServiceType;
    $this->Account = $Account;
    $this->Billing = $Billing;
    $this->SpecialServices = $SpecialServices;
    $this->Currency = $Currency;
    $this->UnitOfMeasurement = $UnitOfMeasurement;
    $this->ShipmentIdentificationNumber = $ShipmentIdentificationNumber;
    $this->UseOwnShipmentIdentificationNumber = $UseOwnShipmentIdentificationNumber;
    $this->PackagesCount = $PackagesCount;
    $this->SendPackage = $SendPackage;
    $this->LabelType = $LabelType;
    $this->LabelTemplate = $LabelTemplate;
    $this->ArchiveLabelTemplate = $ArchiveLabelTemplate;
    $this->PaperlessTradeEnabled = $PaperlessTradeEnabled;
    $this->PaperlessTradeImage = $PaperlessTradeImage;
  }

}
