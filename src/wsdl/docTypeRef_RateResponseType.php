<?php

class docTypeRef_RateResponseType
{

  /**
   * 
   * @var docTypeRef_ProviderType[] $Provider
   * @access public
   */
  public $Provider = null;

  /**
   * 
   * @param docTypeRef_ProviderType[] $Provider
   * @access public
   */
  public function __construct($Provider)
  {
    $this->Provider = $Provider;
  }

}
