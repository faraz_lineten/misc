<?php

class docTypeRef_RequestedShipmentType2
{

  /**
   * 
   * @var DropOffType2 $DropOffType
   * @access public
   */
  public $DropOffType = null;

  /**
   * 
   * @var NextBusinessDay2 $NextBusinessDay
   * @access public
   */
  public $NextBusinessDay = null;

  /**
   * 
   * @var docTypeRef_ShipType2 $Ship
   * @access public
   */
  public $Ship = null;

  /**
   * 
   * @var docTypeRef_PackagesType2 $Packages
   * @access public
   */
  public $Packages = null;

  /**
   * 
   * @var ShipTimestamp2 $ShipTimestamp
   * @access public
   */
  public $ShipTimestamp = null;

  /**
   * 
   * @var UnitOfMeasurement2 $UnitOfMeasurement
   * @access public
   */
  public $UnitOfMeasurement = null;

  /**
   * 
   * @var Content2 $Content
   * @access public
   */
  public $Content = null;

  /**
   * 
   * @var DeclaredValue $DeclaredValue
   * @access public
   */
  public $DeclaredValue = null;

  /**
   * 
   * @var DeclaredValueCurrencyCode $DeclaredValueCurrencyCode
   * @access public
   */
  public $DeclaredValueCurrencyCode = null;

  /**
   * 
   * @var PaymentInfo2 $PaymentInfo
   * @access public
   */
  public $PaymentInfo = null;

  /**
   * 
   * @var Account2 $Account
   * @access public
   */
  public $Account = null;

  /**
   * 
   * @var Billing2 $Billing
   * @access public
   */
  public $Billing = null;

  /**
   * 
   * @var Services2 $SpecialServices
   * @access public
   */
  public $SpecialServices = null;

  /**
   * 
   * @var RequestValueAddedServices $RequestValueAddedServices
   * @access public
   */
  public $RequestValueAddedServices = null;

  /**
   * 
   * @var ServiceType $ServiceType
   * @access public
   */
  public $ServiceType = null;

  /**
   * 
   * @var LocalServiceType $LocalServiceType
   * @access public
   */
  public $LocalServiceType = null;

  /**
   * 
   * @var LandedCostType $LandedCost
   * @access public
   */
  public $LandedCost = null;

  /**
   * 
   * @param DropOffType2 $DropOffType
   * @param NextBusinessDay2 $NextBusinessDay
   * @param docTypeRef_ShipType2 $Ship
   * @param docTypeRef_PackagesType2 $Packages
   * @param ShipTimestamp2 $ShipTimestamp
   * @param UnitOfMeasurement2 $UnitOfMeasurement
   * @param Content2 $Content
   * @param DeclaredValue $DeclaredValue
   * @param DeclaredValueCurrencyCode $DeclaredValueCurrencyCode
   * @param PaymentInfo2 $PaymentInfo
   * @param Account2 $Account
   * @param Billing2 $Billing
   * @param Services2 $SpecialServices
   * @param RequestValueAddedServices $RequestValueAddedServices
   * @param ServiceType $ServiceType
   * @param LocalServiceType $LocalServiceType
   * @param LandedCostType $LandedCost
   * @access public
   */
  public function __construct($DropOffType, $NextBusinessDay, $Ship, $Packages, $ShipTimestamp, $UnitOfMeasurement, $Content, $DeclaredValue, $DeclaredValueCurrencyCode, $PaymentInfo, $Account, $Billing, $SpecialServices, $RequestValueAddedServices, $ServiceType, $LocalServiceType, $LandedCost)
  {
    $this->DropOffType = $DropOffType;
    $this->NextBusinessDay = $NextBusinessDay;
    $this->Ship = $Ship;
    $this->Packages = $Packages;
    $this->ShipTimestamp = $ShipTimestamp;
    $this->UnitOfMeasurement = $UnitOfMeasurement;
    $this->Content = $Content;
    $this->DeclaredValue = $DeclaredValue;
    $this->DeclaredValueCurrencyCode = $DeclaredValueCurrencyCode;
    $this->PaymentInfo = $PaymentInfo;
    $this->Account = $Account;
    $this->Billing = $Billing;
    $this->SpecialServices = $SpecialServices;
    $this->RequestValueAddedServices = $RequestValueAddedServices;
    $this->ServiceType = $ServiceType;
    $this->LocalServiceType = $LocalServiceType;
    $this->LandedCost = $LandedCost;
  }

}
