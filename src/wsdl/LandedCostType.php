<?php

class LandedCostType
{

  /**
   * 
   * @var GetItemCostBreakdown2 $GetItemCostBreakdown
   * @access public
   */
  public $GetItemCostBreakdown = null;

  /**
   * 
   * @var CurrencyCode2 $ShipmentCurrencyCode
   * @access public
   */
  public $ShipmentCurrencyCode = null;

  /**
   * 
   * @var ShipmentPurpose2 $ShipmentPurpose
   * @access public
   */
  public $ShipmentPurpose = null;

  /**
   * 
   * @var ShipmentTransportationMode2 $ShipmentTransportationMode
   * @access public
   */
  public $ShipmentTransportationMode = null;

  /**
   * 
   * @var MerchantSelectedCarrierName2 $MerchantSelectedCarrierName
   * @access public
   */
  public $MerchantSelectedCarrierName = null;

  /**
   * 
   * @var ItemsType $Items
   * @access public
   */
  public $Items = null;

  /**
   * 
   * @var ShipmentMonetaryAmountType $ShipmentMonetaryAmount
   * @access public
   */
  public $ShipmentMonetaryAmount = null;

  /**
   * 
   * @param GetItemCostBreakdown2 $GetItemCostBreakdown
   * @param CurrencyCode2 $ShipmentCurrencyCode
   * @param ShipmentPurpose2 $ShipmentPurpose
   * @param ShipmentTransportationMode2 $ShipmentTransportationMode
   * @param MerchantSelectedCarrierName2 $MerchantSelectedCarrierName
   * @param ItemsType $Items
   * @param ShipmentMonetaryAmountType $ShipmentMonetaryAmount
   * @access public
   */
  public function __construct($GetItemCostBreakdown, $ShipmentCurrencyCode, $ShipmentPurpose, $ShipmentTransportationMode, $MerchantSelectedCarrierName, $Items, $ShipmentMonetaryAmount)
  {
    $this->GetItemCostBreakdown = $GetItemCostBreakdown;
    $this->ShipmentCurrencyCode = $ShipmentCurrencyCode;
    $this->ShipmentPurpose = $ShipmentPurpose;
    $this->ShipmentTransportationMode = $ShipmentTransportationMode;
    $this->MerchantSelectedCarrierName = $MerchantSelectedCarrierName;
    $this->Items = $Items;
    $this->ShipmentMonetaryAmount = $ShipmentMonetaryAmount;
  }

}
