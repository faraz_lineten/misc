<?php

class docTypeRef_ContactType1
{

  /**
   * 
   * @var PersonName $PersonName
   * @access public
   */
  public $PersonName = null;

  /**
   * 
   * @var CompanyName $CompanyName
   * @access public
   */
  public $CompanyName = null;

  /**
   * 
   * @var PhoneNumber $PhoneNumber
   * @access public
   */
  public $PhoneNumber = null;

  /**
   * 
   * @var EmailAddress $EmailAddress
   * @access public
   */
  public $EmailAddress = null;

  /**
   * 
   * @var MobilePhoneNumber $MobilePhoneNumber
   * @access public
   */
  public $MobilePhoneNumber = null;

  /**
   * 
   * @param PersonName $PersonName
   * @param CompanyName $CompanyName
   * @param PhoneNumber $PhoneNumber
   * @param EmailAddress $EmailAddress
   * @param MobilePhoneNumber $MobilePhoneNumber
   * @access public
   */
  public function __construct($PersonName, $CompanyName, $PhoneNumber, $EmailAddress, $MobilePhoneNumber)
  {
    $this->PersonName = $PersonName;
    $this->CompanyName = $CompanyName;
    $this->PhoneNumber = $PhoneNumber;
    $this->EmailAddress = $EmailAddress;
    $this->MobilePhoneNumber = $MobilePhoneNumber;
  }

}
