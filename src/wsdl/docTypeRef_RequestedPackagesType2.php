<?php

class docTypeRef_RequestedPackagesType2
{

  /**
   * 
   * @var docTypeRef_WeightType $Weight
   * @access public
   */
  public $Weight = null;

  /**
   * 
   * @var docTypeRef_DimensionsType2 $Dimensions
   * @access public
   */
  public $Dimensions = null;

  /**
   * 
   * @var _x0040_number3 $number
   * @access public
   */
  public $number = null;

  /**
   * 
   * @param docTypeRef_WeightType $Weight
   * @param docTypeRef_DimensionsType2 $Dimensions
   * @param _x0040_number3 $number
   * @access public
   */
  public function __construct($Weight, $Dimensions, $number)
  {
    $this->Weight = $Weight;
    $this->Dimensions = $Dimensions;
    $this->number = $number;
  }

}
