<?php

class docTypeRef_ServiceType
{

  /**
   * 
   * @var docTypeRef_TotalNetType $TotalNet
   * @access public
   */
  public $TotalNet = null;

  /**
   * 
   * @var docTypeRef_ChargesType $Charges
   * @access public
   */
  public $Charges = null;

  /**
   * 
   * @var docTypeRef_ItemsType $Items
   * @access public
   */
  public $Items = null;

  /**
   * 
   * @var dateTime $DeliveryTime
   * @access public
   */
  public $DeliveryTime = null;

  /**
   * 
   * @var dateTime $CutoffTime
   * @access public
   */
  public $CutoffTime = null;

  /**
   * 
   * @var NextBusinessDayInd2 $NextBusinessDayInd
   * @access public
   */
  public $NextBusinessDayInd = null;

  /**
   * 
   * @var _x0040_type $type
   * @access public
   */
  public $type = null;

  /**
   * 
   * @var _x0040_account $account
   * @access public
   */
  public $account = null;

  /**
   * 
   * @param docTypeRef_TotalNetType $TotalNet
   * @param docTypeRef_ChargesType $Charges
   * @param docTypeRef_ItemsType $Items
   * @param dateTime $DeliveryTime
   * @param dateTime $CutoffTime
   * @param NextBusinessDayInd2 $NextBusinessDayInd
   * @param _x0040_type $type
   * @param _x0040_account $account
   * @access public
   */
  public function __construct($TotalNet, $Charges, $Items, $DeliveryTime, $CutoffTime, $NextBusinessDayInd, $type, $account)
  {
    $this->TotalNet = $TotalNet;
    $this->Charges = $Charges;
    $this->Items = $Items;
    $this->DeliveryTime = $DeliveryTime;
    $this->CutoffTime = $CutoffTime;
    $this->NextBusinessDayInd = $NextBusinessDayInd;
    $this->type = $type;
    $this->account = $account;
  }

}
