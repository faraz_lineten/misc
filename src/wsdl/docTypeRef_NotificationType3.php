<?php

class docTypeRef_NotificationType3
{

  /**
   * 
   * @var Message2 $Message
   * @access public
   */
  public $Message = null;

  /**
   * 
   * @var _x0040_code4 $code
   * @access public
   */
  public $code = null;

  /**
   * 
   * @var anonymous205 $src
   * @access public
   */
  public $src = null;

  /**
   * 
   * @param Message2 $Message
   * @param _x0040_code4 $code
   * @param anonymous205 $src
   * @access public
   */
  public function __construct($Message, $code, $src)
  {
    $this->Message = $Message;
    $this->code = $code;
    $this->src = $src;
  }

}
