<?php

class docTypeRef_ProviderType
{

  /**
   * 
   * @var ServiceHeaderType $ServiceHeader
   * @access public
   */
  public $ServiceHeader = null;

  /**
   * 
   * @var docTypeRef_NotificationType3 $Notification
   * @access public
   */
  public $Notification = null;

  /**
   * 
   * @var docTypeRef_ServiceType[] $Service
   * @access public
   */
  public $Service = null;

  /**
   * 
   * @var _x0040_code3 $code
   * @access public
   */
  public $code = null;

  /**
   * 
   * @param ServiceHeaderType $ServiceHeader
   * @param docTypeRef_NotificationType3 $Notification
   * @param docTypeRef_ServiceType[] $Service
   * @param _x0040_code3 $code
   * @access public
   */
  public function __construct($ServiceHeader, $Notification, $Service, $code)
  {
    $this->ServiceHeader = $ServiceHeader;
    $this->Notification = $Notification;
    $this->Service = $Service;
    $this->code = $code;
  }

}
