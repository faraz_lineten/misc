<?php

class docTypeRef_RegistrationNumbers
{

  /**
   * 
   * @var docTypeRef_RegistrationNumber $RegistrationNumber
   * @access public
   */
  public $RegistrationNumber = null;

  /**
   * 
   * @param docTypeRef_RegistrationNumber $RegistrationNumber
   * @access public
   */
  public function __construct($RegistrationNumber)
  {
    $this->RegistrationNumber = $RegistrationNumber;
  }

}
