<?php

class docTypeRef_DimensionsType2
{

  /**
   * 
   * @var Length2 $Length
   * @access public
   */
  public $Length = null;

  /**
   * 
   * @var Width2 $Width
   * @access public
   */
  public $Width = null;

  /**
   * 
   * @var Height2 $Height
   * @access public
   */
  public $Height = null;

  /**
   * 
   * @param Length2 $Length
   * @param Width2 $Width
   * @param Height2 $Height
   * @access public
   */
  public function __construct($Length, $Width, $Height)
  {
    $this->Length = $Length;
    $this->Width = $Width;
    $this->Height = $Height;
  }

}
