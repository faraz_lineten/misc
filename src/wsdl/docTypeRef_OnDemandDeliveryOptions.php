<?php

class docTypeRef_OnDemandDeliveryOptions
{

  /**
   * 
   * @var DeliveryOption $DeliveryOption
   * @access public
   */
  public $DeliveryOption = null;

  /**
   * 
   * @var Location $Location
   * @access public
   */
  public $Location = null;

  /**
   * 
   * @var Instructions $Instructions
   * @access public
   */
  public $Instructions = null;

  /**
   * 
   * @var GateCode $GateCode
   * @access public
   */
  public $GateCode = null;

  /**
   * 
   * @var LWNTypeCode $LWNTypeCode
   * @access public
   */
  public $LWNTypeCode = null;

  /**
   * 
   * @var NeighbourName $NeighbourName
   * @access public
   */
  public $NeighbourName = null;

  /**
   * 
   * @var NeighbourHouseNumber $NeighbourHouseNumber
   * @access public
   */
  public $NeighbourHouseNumber = null;

  /**
   * 
   * @var AuthorizerName $AuthorizerName
   * @access public
   */
  public $AuthorizerName = null;

  /**
   * 
   * @var SelectedServicePointID $SelectedServicePointID
   * @access public
   */
  public $SelectedServicePointID = null;

  /**
   * 
   * @var RequestedDeliveryDate $RequestedDeliveryDate
   * @access public
   */
  public $RequestedDeliveryDate = null;

  /**
   * 
   * @param DeliveryOption $DeliveryOption
   * @param Location $Location
   * @param Instructions $Instructions
   * @param GateCode $GateCode
   * @param LWNTypeCode $LWNTypeCode
   * @param NeighbourName $NeighbourName
   * @param NeighbourHouseNumber $NeighbourHouseNumber
   * @param AuthorizerName $AuthorizerName
   * @param SelectedServicePointID $SelectedServicePointID
   * @param RequestedDeliveryDate $RequestedDeliveryDate
   * @access public
   */
  public function __construct($DeliveryOption, $Location, $Instructions, $GateCode, $LWNTypeCode, $NeighbourName, $NeighbourHouseNumber, $AuthorizerName, $SelectedServicePointID, $RequestedDeliveryDate)
  {
    $this->DeliveryOption = $DeliveryOption;
    $this->Location = $Location;
    $this->Instructions = $Instructions;
    $this->GateCode = $GateCode;
    $this->LWNTypeCode = $LWNTypeCode;
    $this->NeighbourName = $NeighbourName;
    $this->NeighbourHouseNumber = $NeighbourHouseNumber;
    $this->AuthorizerName = $AuthorizerName;
    $this->SelectedServicePointID = $SelectedServicePointID;
    $this->RequestedDeliveryDate = $RequestedDeliveryDate;
  }

}
