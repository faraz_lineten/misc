<?php

class docTypeRef_RequestedShipmentType
{

  /**
   * 
   * @var docTypeRef_ShipmentInfoType $ShipmentInfo
   * @access public
   */
  public $ShipmentInfo = null;

  /**
   * 
   * @var ShipTimestamp $ShipTimestamp
   * @access public
   */
  public $ShipTimestamp = null;

  /**
   * 
   * @var PickupLocationCloseTime $PickupLocationCloseTime
   * @access public
   */
  public $PickupLocationCloseTime = null;

  /**
   * 
   * @var SpecialPickupInstruction $SpecialPickupInstruction
   * @access public
   */
  public $SpecialPickupInstruction = null;

  /**
   * 
   * @var PickupLocation $PickupLocation
   * @access public
   */
  public $PickupLocation = null;

  /**
   * 
   * @var PaymentInfo $PaymentInfo
   * @access public
   */
  public $PaymentInfo = null;

  /**
   * 
   * @var docTypeRef_InternationDetailType $InternationalDetail
   * @access public
   */
  public $InternationalDetail = null;

  /**
   * 
   * @var docTypeRef_OnDemandDeliveryOptions $OnDemandDeliveryOptions
   * @access public
   */
  public $OnDemandDeliveryOptions = null;

  /**
   * 
   * @var docTypeRef_ShipType $Ship
   * @access public
   */
  public $Ship = null;

  /**
   * 
   * @var docTypeRef_PackagesType $Packages
   * @access public
   */
  public $Packages = null;

  /**
   * 
   * @var docTypeRef_DangerousGoods $DangerousGoods
   * @access public
   */
  public $DangerousGoods = null;

  /**
   * 
   * @param docTypeRef_ShipmentInfoType $ShipmentInfo
   * @param ShipTimestamp $ShipTimestamp
   * @param PickupLocationCloseTime $PickupLocationCloseTime
   * @param SpecialPickupInstruction $SpecialPickupInstruction
   * @param PickupLocation $PickupLocation
   * @param PaymentInfo $PaymentInfo
   * @param docTypeRef_InternationDetailType $InternationalDetail
   * @param docTypeRef_OnDemandDeliveryOptions $OnDemandDeliveryOptions
   * @param docTypeRef_ShipType $Ship
   * @param docTypeRef_PackagesType $Packages
   * @param docTypeRef_DangerousGoods $DangerousGoods
   * @access public
   */
  public function __construct($ShipmentInfo, $ShipTimestamp, $PickupLocationCloseTime, $SpecialPickupInstruction, $PickupLocation, $PaymentInfo, $InternationalDetail, $OnDemandDeliveryOptions, $Ship, $Packages, $DangerousGoods)
  {
    $this->ShipmentInfo = $ShipmentInfo;
    $this->ShipTimestamp = $ShipTimestamp;
    $this->PickupLocationCloseTime = $PickupLocationCloseTime;
    $this->SpecialPickupInstruction = $SpecialPickupInstruction;
    $this->PickupLocation = $PickupLocation;
    $this->PaymentInfo = $PaymentInfo;
    $this->InternationalDetail = $InternationalDetail;
    $this->OnDemandDeliveryOptions = $OnDemandDeliveryOptions;
    $this->Ship = $Ship;
    $this->Packages = $Packages;
    $this->DangerousGoods = $DangerousGoods;
  }

}
