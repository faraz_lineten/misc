<?php

class docTypeRef_PackagesType
{

  /**
   * 
   * @var docTypeRef_RequestedPackagesType $RequestedPackages
   * @access public
   */
  public $RequestedPackages = null;

  /**
   * 
   * @param docTypeRef_RequestedPackagesType $RequestedPackages
   * @access public
   */
  public function __construct($RequestedPackages)
  {
    $this->RequestedPackages = $RequestedPackages;
  }

}
