<?php

class Service2
{

  /**
   * 
   * @var ServiceType2 $ServiceType
   * @access public
   */
  public $ServiceType = null;

  /**
   * 
   * @var Money2 $ServiceValue
   * @access public
   */
  public $ServiceValue = null;

  /**
   * 
   * @var CurrencyCode2 $CurrencyCode
   * @access public
   */
  public $CurrencyCode = null;

  /**
   * 
   * @var PaymentCode2 $PaymentCode
   * @access public
   */
  public $PaymentCode = null;

  /**
   * 
   * @var date $StartDate
   * @access public
   */
  public $StartDate = null;

  /**
   * 
   * @var date $EndDate
   * @access public
   */
  public $EndDate = null;

  /**
   * 
   * @var TextType2 $TextInstruction
   * @access public
   */
  public $TextInstruction = null;

  /**
   * 
   * @param ServiceType2 $ServiceType
   * @param Money2 $ServiceValue
   * @param CurrencyCode2 $CurrencyCode
   * @param PaymentCode2 $PaymentCode
   * @param date $StartDate
   * @param date $EndDate
   * @param TextType2 $TextInstruction
   * @access public
   */
  public function __construct($ServiceType, $ServiceValue, $CurrencyCode, $PaymentCode, $StartDate, $EndDate, $TextInstruction)
  {
    $this->ServiceType = $ServiceType;
    $this->ServiceValue = $ServiceValue;
    $this->CurrencyCode = $CurrencyCode;
    $this->PaymentCode = $PaymentCode;
    $this->StartDate = $StartDate;
    $this->EndDate = $EndDate;
    $this->TextInstruction = $TextInstruction;
  }

}
