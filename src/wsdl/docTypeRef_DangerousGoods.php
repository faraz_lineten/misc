<?php

class docTypeRef_DangerousGoods
{

  /**
   * 
   * @var docTypeRef_Content $Content
   * @access public
   */
  public $Content = null;

  /**
   * 
   * @param docTypeRef_Content $Content
   * @access public
   */
  public function __construct($Content)
  {
    $this->Content = $Content;
  }

}
