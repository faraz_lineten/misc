<?php

class docTypeRef_DeleteResponseType
{

  /**
   * 
   * @var docTypeRef_NotificationType $Notification
   * @access public
   */
  public $Notification = null;

  /**
   * 
   * @param docTypeRef_NotificationType $Notification
   * @access public
   */
  public function __construct($Notification)
  {
    $this->Notification = $Notification;
  }

}
