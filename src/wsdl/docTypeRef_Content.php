<?php

class docTypeRef_Content
{

  /**
   * 
   * @var ContentID $ContentID
   * @access public
   */
  public $ContentID = null;

  /**
   * 
   * @var DryIceTotalNetWeight $DryIceTotalNetWeight
   * @access public
   */
  public $DryIceTotalNetWeight = null;

  /**
   * 
   * @var UNCode $UNCode
   * @access public
   */
  public $UNCode = null;

  /**
   * 
   * @param ContentID $ContentID
   * @param DryIceTotalNetWeight $DryIceTotalNetWeight
   * @param UNCode $UNCode
   * @access public
   */
  public function __construct($ContentID, $DryIceTotalNetWeight, $UNCode)
  {
    $this->ContentID = $ContentID;
    $this->DryIceTotalNetWeight = $DryIceTotalNetWeight;
    $this->UNCode = $UNCode;
  }

}
