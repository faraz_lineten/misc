<?php

class docTypeRef_PackageResultType
{

  /**
   * 
   * @var TrackingNumber $TrackingNumber
   * @access public
   */
  public $TrackingNumber = null;

  /**
   * 
   * @var _x0040_number2 $number
   * @access public
   */
  public $number = null;

  /**
   * 
   * @param TrackingNumber $TrackingNumber
   * @param _x0040_number2 $number
   * @access public
   */
  public function __construct($TrackingNumber, $number)
  {
    $this->TrackingNumber = $TrackingNumber;
    $this->number = $number;
  }

}
