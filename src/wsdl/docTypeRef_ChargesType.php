<?php

class docTypeRef_ChargesType
{

  /**
   * 
   * @var string $Currency
   * @access public
   */
  public $Currency = null;

  /**
   * 
   * @var docTypeRef_ChargeType[] $Charge
   * @access public
   */
  public $Charge = null;

  /**
   * 
   * @param string $Currency
   * @param docTypeRef_ChargeType[] $Charge
   * @access public
   */
  public function __construct($Currency, $Charge)
  {
    $this->Currency = $Currency;
    $this->Charge = $Charge;
  }

}
