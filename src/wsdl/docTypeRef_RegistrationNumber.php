<?php

class docTypeRef_RegistrationNumber
{

  /**
   * 
   * @var Number $Number
   * @access public
   */
  public $Number = null;

  /**
   * 
   * @var NumberTypeCode $NumberTypeCode
   * @access public
   */
  public $NumberTypeCode = null;

  /**
   * 
   * @var NumberIssuerCountryCode $NumberIssuerCountryCode
   * @access public
   */
  public $NumberIssuerCountryCode = null;

  /**
   * 
   * @param Number $Number
   * @param NumberTypeCode $NumberTypeCode
   * @param NumberIssuerCountryCode $NumberIssuerCountryCode
   * @access public
   */
  public function __construct($Number, $NumberTypeCode, $NumberIssuerCountryCode)
  {
    $this->Number = $Number;
    $this->NumberTypeCode = $NumberTypeCode;
    $this->NumberIssuerCountryCode = $NumberIssuerCountryCode;
  }

}
