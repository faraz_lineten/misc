<?php

class docTypeRef_ChargeType
{

  /**
   * 
   * @var ChargeCode $ChargeCode
   * @access public
   */
  public $ChargeCode = null;

  /**
   * 
   * @var string $ChargeType
   * @access public
   */
  public $ChargeType = null;

  /**
   * 
   * @var string $ChargeAmount
   * @access public
   */
  public $ChargeAmount = null;

  /**
   * 
   * @var ChargeName $ChargeName
   * @access public
   */
  public $ChargeName = null;

  /**
   * 
   * @var ChargeCurrencyCode $ChargeCurrencyCode
   * @access public
   */
  public $ChargeCurrencyCode = null;

  /**
   * 
   * @param ChargeCode $ChargeCode
   * @param string $ChargeType
   * @param string $ChargeAmount
   * @param ChargeName $ChargeName
   * @param ChargeCurrencyCode $ChargeCurrencyCode
   * @access public
   */
  public function __construct($ChargeCode, $ChargeType, $ChargeAmount, $ChargeName, $ChargeCurrencyCode)
  {
    $this->ChargeCode = $ChargeCode;
    $this->ChargeType = $ChargeType;
    $this->ChargeAmount = $ChargeAmount;
    $this->ChargeName = $ChargeName;
    $this->ChargeCurrencyCode = $ChargeCurrencyCode;
  }

}
