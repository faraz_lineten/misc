<?php

class ServiceHeaderType
{

  /**
   * 
   * @var dateTime $MessageTime
   * @access public
   */
  public $MessageTime = null;

  /**
   * 
   * @var MessageReference32 $MessageReference
   * @access public
   */
  public $MessageReference = null;

  /**
   * 
   * @param dateTime $MessageTime
   * @param MessageReference32 $MessageReference
   * @access public
   */
  public function __construct($MessageTime, $MessageReference)
  {
    $this->MessageTime = $MessageTime;
    $this->MessageReference = $MessageReference;
  }

}
