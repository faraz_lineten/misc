<?php

class Billing2
{

  /**
   * 
   * @var Account2 $ShipperAccountNumber
   * @access public
   */
  public $ShipperAccountNumber = null;

  /**
   * 
   * @var ShipmentPaymentType2 $ShippingPaymentType
   * @access public
   */
  public $ShippingPaymentType = null;

  /**
   * 
   * @var Account2 $BillingAccountNumber
   * @access public
   */
  public $BillingAccountNumber = null;

  /**
   * 
   * @param Account2 $ShipperAccountNumber
   * @param ShipmentPaymentType2 $ShippingPaymentType
   * @param Account2 $BillingAccountNumber
   * @access public
   */
  public function __construct($ShipperAccountNumber, $ShippingPaymentType, $BillingAccountNumber)
  {
    $this->ShipperAccountNumber = $ShipperAccountNumber;
    $this->ShippingPaymentType = $ShippingPaymentType;
    $this->BillingAccountNumber = $BillingAccountNumber;
  }

}
