<?php

class docTypeRef_ProcessShipmentRequestType
{

  /**
   * 
   * @var MessageId $MessageId
   * @access public
   */
  public $MessageId = null;

  /**
   * 
   * @var docTypeRef_ClientDetailType2 $ClientDetail
   * @access public
   */
  public $ClientDetail = null;

  /**
   * 
   * @var docTypeRef_RequestedShipmentType $RequestedShipment
   * @access public
   */
  public $RequestedShipment = null;

  /**
   * 
   * @param MessageId $MessageId
   * @param docTypeRef_ClientDetailType2 $ClientDetail
   * @param docTypeRef_RequestedShipmentType $RequestedShipment
   * @access public
   */
  public function __construct($MessageId, $ClientDetail, $RequestedShipment)
  {
    $this->MessageId = $MessageId;
    $this->ClientDetail = $ClientDetail;
    $this->RequestedShipment = $RequestedShipment;
  }

}
