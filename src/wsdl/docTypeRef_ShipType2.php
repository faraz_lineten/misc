<?php

class docTypeRef_ShipType2
{

  /**
   * 
   * @var docTypeRef_AddressType2 $Shipper
   * @access public
   */
  public $Shipper = null;

  /**
   * 
   * @var docTypeRef_AddressType2 $Recipient
   * @access public
   */
  public $Recipient = null;

  /**
   * 
   * @param docTypeRef_AddressType2 $Shipper
   * @param docTypeRef_AddressType2 $Recipient
   * @access public
   */
  public function __construct($Shipper, $Recipient)
  {
    $this->Shipper = $Shipper;
    $this->Recipient = $Recipient;
  }

}
