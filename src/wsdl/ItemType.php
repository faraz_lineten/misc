<?php

class ItemType
{

  /**
   * 
   * @var int $ItemNumber
   * @access public
   */
  public $ItemNumber = null;

  /**
   * 
   * @var Description2 $Description
   * @access public
   */
  public $Description = null;

  /**
   * 
   * @var Remark2 $Remark
   * @access public
   */
  public $Remark = null;

  /**
   * 
   * @var CountryCode2 $ManufacturingCountryCode
   * @access public
   */
  public $ManufacturingCountryCode = null;

  /**
   * 
   * @var SKUPartNumber2 $SKUPartNumber
   * @access public
   */
  public $SKUPartNumber = null;

  /**
   * 
   * @var Value $Quantity
   * @access public
   */
  public $Quantity = null;

  /**
   * 
   * @var UnitOfMeasurement3 $QuantityType
   * @access public
   */
  public $QuantityType = null;

  /**
   * 
   * @var Value $AdditionalQuantity
   * @access public
   */
  public $AdditionalQuantity = null;

  /**
   * 
   * @var UnitOfMeasurement3 $AdditionalQuantityType
   * @access public
   */
  public $AdditionalQuantityType = null;

  /**
   * 
   * @var Money2 $UnitPrice
   * @access public
   */
  public $UnitPrice = null;

  /**
   * 
   * @var CurrencyCode2 $UnitPriceCurrencyCode
   * @access public
   */
  public $UnitPriceCurrencyCode = null;

  /**
   * 
   * @var Money2 $CustomsValue
   * @access public
   */
  public $CustomsValue = null;

  /**
   * 
   * @var CurrencyCode2 $CustomsValueCurrencyCode
   * @access public
   */
  public $CustomsValueCurrencyCode = null;

  /**
   * 
   * @var HarmonizedSystemCode $HarmonizedSystemCode
   * @access public
   */
  public $HarmonizedSystemCode = null;

  /**
   * 
   * @var Value $ItemWeight
   * @access public
   */
  public $ItemWeight = null;

  /**
   * 
   * @var UnitOfMeasurement2 $ItemWeightUnitofMeasurement
   * @access public
   */
  public $ItemWeightUnitofMeasurement = null;

  /**
   * 
   * @var Category $Category
   * @access public
   */
  public $Category = null;

  /**
   * 
   * @var Brand $Brand
   * @access public
   */
  public $Brand = null;

  /**
   * 
   * @var GoodsCharacteristicsType $GoodsCharacteristics
   * @access public
   */
  public $GoodsCharacteristics = null;

  /**
   * 
   * @param int $ItemNumber
   * @param Description2 $Description
   * @param Remark2 $Remark
   * @param CountryCode2 $ManufacturingCountryCode
   * @param SKUPartNumber2 $SKUPartNumber
   * @param Value $Quantity
   * @param UnitOfMeasurement3 $QuantityType
   * @param Value $AdditionalQuantity
   * @param UnitOfMeasurement3 $AdditionalQuantityType
   * @param Money2 $UnitPrice
   * @param CurrencyCode2 $UnitPriceCurrencyCode
   * @param Money2 $CustomsValue
   * @param CurrencyCode2 $CustomsValueCurrencyCode
   * @param HarmonizedSystemCode $HarmonizedSystemCode
   * @param Value $ItemWeight
   * @param UnitOfMeasurement2 $ItemWeightUnitofMeasurement
   * @param Category $Category
   * @param Brand $Brand
   * @param GoodsCharacteristicsType $GoodsCharacteristics
   * @access public
   */
  public function __construct($ItemNumber, $Description, $Remark, $ManufacturingCountryCode, $SKUPartNumber, $Quantity, $QuantityType, $AdditionalQuantity, $AdditionalQuantityType, $UnitPrice, $UnitPriceCurrencyCode, $CustomsValue, $CustomsValueCurrencyCode, $HarmonizedSystemCode, $ItemWeight, $ItemWeightUnitofMeasurement, $Category, $Brand, $GoodsCharacteristics)
  {
    $this->ItemNumber = $ItemNumber;
    $this->Description = $Description;
    $this->Remark = $Remark;
    $this->ManufacturingCountryCode = $ManufacturingCountryCode;
    $this->SKUPartNumber = $SKUPartNumber;
    $this->Quantity = $Quantity;
    $this->QuantityType = $QuantityType;
    $this->AdditionalQuantity = $AdditionalQuantity;
    $this->AdditionalQuantityType = $AdditionalQuantityType;
    $this->UnitPrice = $UnitPrice;
    $this->UnitPriceCurrencyCode = $UnitPriceCurrencyCode;
    $this->CustomsValue = $CustomsValue;
    $this->CustomsValueCurrencyCode = $CustomsValueCurrencyCode;
    $this->HarmonizedSystemCode = $HarmonizedSystemCode;
    $this->ItemWeight = $ItemWeight;
    $this->ItemWeightUnitofMeasurement = $ItemWeightUnitofMeasurement;
    $this->Category = $Category;
    $this->Brand = $Brand;
    $this->GoodsCharacteristics = $GoodsCharacteristics;
  }

}
