<?php

class docTypeRef_CommoditiesType
{

  /**
   * 
   * @var NumberOfPieces $NumberOfPieces
   * @access public
   */
  public $NumberOfPieces = null;

  /**
   * 
   * @var Description $Description
   * @access public
   */
  public $Description = null;

  /**
   * 
   * @var CountryOfManufacture $CountryOfManufacture
   * @access public
   */
  public $CountryOfManufacture = null;

  /**
   * 
   * @var Quantity $Quantity
   * @access public
   */
  public $Quantity = null;

  /**
   * 
   * @var UnitPrice $UnitPrice
   * @access public
   */
  public $UnitPrice = null;

  /**
   * 
   * @var CustomsValue $CustomsValue
   * @access public
   */
  public $CustomsValue = null;

  /**
   * 
   * @var USFilingTypeValue $USFilingTypeValue
   * @access public
   */
  public $USFilingTypeValue = null;

  /**
   * 
   * @param NumberOfPieces $NumberOfPieces
   * @param Description $Description
   * @param CountryOfManufacture $CountryOfManufacture
   * @param Quantity $Quantity
   * @param UnitPrice $UnitPrice
   * @param CustomsValue $CustomsValue
   * @param USFilingTypeValue $USFilingTypeValue
   * @access public
   */
  public function __construct($NumberOfPieces, $Description, $CountryOfManufacture, $Quantity, $UnitPrice, $CustomsValue, $USFilingTypeValue)
  {
    $this->NumberOfPieces = $NumberOfPieces;
    $this->Description = $Description;
    $this->CountryOfManufacture = $CountryOfManufacture;
    $this->Quantity = $Quantity;
    $this->UnitPrice = $UnitPrice;
    $this->CustomsValue = $CustomsValue;
    $this->USFilingTypeValue = $USFilingTypeValue;
  }

}
