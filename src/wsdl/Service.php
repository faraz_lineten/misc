<?php

class Service
{

  /**
   * 
   * @var ServiceTypeCode $ServiceType
   * @access public
   */
  public $ServiceType = null;

  /**
   * 
   * @var Money $ServiceValue
   * @access public
   */
  public $ServiceValue = null;

  /**
   * 
   * @var CurrencyCode $CurrencyCode
   * @access public
   */
  public $CurrencyCode = null;

  /**
   * 
   * @var PaymentCode $PaymentCode
   * @access public
   */
  public $PaymentCode = null;

  /**
   * 
   * @var date $StartDate
   * @access public
   */
  public $StartDate = null;

  /**
   * 
   * @var date $EndDate
   * @access public
   */
  public $EndDate = null;

  /**
   * 
   * @var TextType $TextInstruction
   * @access public
   */
  public $TextInstruction = null;

  /**
   * 
   * @param ServiceTypeCode $ServiceType
   * @param Money $ServiceValue
   * @param CurrencyCode $CurrencyCode
   * @param PaymentCode $PaymentCode
   * @param date $StartDate
   * @param date $EndDate
   * @param TextType $TextInstruction
   * @access public
   */
  public function __construct($ServiceType, $ServiceValue, $CurrencyCode, $PaymentCode, $StartDate, $EndDate, $TextInstruction)
  {
    $this->ServiceType = $ServiceType;
    $this->ServiceValue = $ServiceValue;
    $this->CurrencyCode = $CurrencyCode;
    $this->PaymentCode = $PaymentCode;
    $this->StartDate = $StartDate;
    $this->EndDate = $EndDate;
    $this->TextInstruction = $TextInstruction;
  }

}
