<?php

class docTypeRef_ShipType
{

  /**
   * 
   * @var docTypeRef_ContactInfoType $Shipper
   * @access public
   */
  public $Shipper = null;

  /**
   * 
   * @var docTypeRef_ContactInfoType $Pickup
   * @access public
   */
  public $Pickup = null;

  /**
   * 
   * @var docTypeRef_ContactInfoType $BookingRequestor
   * @access public
   */
  public $BookingRequestor = null;

  /**
   * 
   * @var docTypeRef_ContactInfoType1 $Buyer
   * @access public
   */
  public $Buyer = null;

  /**
   * 
   * @var docTypeRef_ContactInfoType $Recipient
   * @access public
   */
  public $Recipient = null;

  /**
   * 
   * @param docTypeRef_ContactInfoType $Shipper
   * @param docTypeRef_ContactInfoType $Pickup
   * @param docTypeRef_ContactInfoType $BookingRequestor
   * @param docTypeRef_ContactInfoType1 $Buyer
   * @param docTypeRef_ContactInfoType $Recipient
   * @access public
   */
  public function __construct($Shipper, $Pickup, $BookingRequestor, $Buyer, $Recipient)
  {
    $this->Shipper = $Shipper;
    $this->Pickup = $Pickup;
    $this->BookingRequestor = $BookingRequestor;
    $this->Buyer = $Buyer;
    $this->Recipient = $Recipient;
  }

}
