<?php

class docTypeRef_ContactInfoType
{

  /**
   * 
   * @var docTypeRef_ContactType $Contact
   * @access public
   */
  public $Contact = null;

  /**
   * 
   * @var docTypeRef_AddressType $Address
   * @access public
   */
  public $Address = null;

  /**
   * 
   * @var docTypeRef_RegistrationNumbers $RegistrationNumbers
   * @access public
   */
  public $RegistrationNumbers = null;

  /**
   * 
   * @param docTypeRef_ContactType $Contact
   * @param docTypeRef_AddressType $Address
   * @param docTypeRef_RegistrationNumbers $RegistrationNumbers
   * @access public
   */
  public function __construct($Contact, $Address, $RegistrationNumbers)
  {
    $this->Contact = $Contact;
    $this->Address = $Address;
    $this->RegistrationNumbers = $RegistrationNumbers;
  }

}
