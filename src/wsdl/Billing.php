<?php

class Billing
{

  /**
   * 
   * @var Account $ShipperAccountNumber
   * @access public
   */
  public $ShipperAccountNumber = null;

  /**
   * 
   * @var ShipmentPaymentType $ShippingPaymentType
   * @access public
   */
  public $ShippingPaymentType = null;

  /**
   * 
   * @var Account $BillingAccountNumber
   * @access public
   */
  public $BillingAccountNumber = null;

  /**
   * 
   * @param Account $ShipperAccountNumber
   * @param ShipmentPaymentType $ShippingPaymentType
   * @param Account $BillingAccountNumber
   * @access public
   */
  public function __construct($ShipperAccountNumber, $ShippingPaymentType, $BillingAccountNumber)
  {
    $this->ShipperAccountNumber = $ShipperAccountNumber;
    $this->ShippingPaymentType = $ShippingPaymentType;
    $this->BillingAccountNumber = $BillingAccountNumber;
  }

}
