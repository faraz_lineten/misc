<?php

class docTypeRef_PackagesType2
{

  /**
   * 
   * @var docTypeRef_RequestedPackagesType2[] $RequestedPackages
   * @access public
   */
  public $RequestedPackages = null;

  /**
   * 
   * @param docTypeRef_RequestedPackagesType2[] $RequestedPackages
   * @access public
   */
  public function __construct($RequestedPackages)
  {
    $this->RequestedPackages = $RequestedPackages;
  }

}
