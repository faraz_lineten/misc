<?php

class docTypeRef_RequestedPackagesType
{

  /**
   * 
   * @var InsuredValue $InsuredValue
   * @access public
   */
  public $InsuredValue = null;

  /**
   * 
   * @var Weight $Weight
   * @access public
   */
  public $Weight = null;

  /**
   * 
   * @var PieceIdentificationNumber $PieceIdentificationNumber
   * @access public
   */
  public $PieceIdentificationNumber = null;

  /**
   * 
   * @var UseOwnPieceIdentificationNumber $UseOwnPieceIdentificationNumber
   * @access public
   */
  public $UseOwnPieceIdentificationNumber = null;

  /**
   * 
   * @var PackageContentDescription $PackageContentDescription
   * @access public
   */
  public $PackageContentDescription = null;

  /**
   * 
   * @var docTypeRef_DimensionsType $Dimensions
   * @access public
   */
  public $Dimensions = null;

  /**
   * 
   * @var CustomerReferences $CustomerReferences
   * @access public
   */
  public $CustomerReferences = null;

  /**
   * 
   * @var _x0040_number $number
   * @access public
   */
  public $number = null;

  /**
   * 
   * @param InsuredValue $InsuredValue
   * @param Weight $Weight
   * @param PieceIdentificationNumber $PieceIdentificationNumber
   * @param UseOwnPieceIdentificationNumber $UseOwnPieceIdentificationNumber
   * @param PackageContentDescription $PackageContentDescription
   * @param docTypeRef_DimensionsType $Dimensions
   * @param CustomerReferences $CustomerReferences
   * @param _x0040_number $number
   * @access public
   */
  public function __construct($InsuredValue, $Weight, $PieceIdentificationNumber, $UseOwnPieceIdentificationNumber, $PackageContentDescription, $Dimensions, $CustomerReferences, $number)
  {
    $this->InsuredValue = $InsuredValue;
    $this->Weight = $Weight;
    $this->PieceIdentificationNumber = $PieceIdentificationNumber;
    $this->UseOwnPieceIdentificationNumber = $UseOwnPieceIdentificationNumber;
    $this->PackageContentDescription = $PackageContentDescription;
    $this->Dimensions = $Dimensions;
    $this->CustomerReferences = $CustomerReferences;
    $this->number = $number;
  }

}
