<?php

class docTypeRef_AddressType1
{

  /**
   * 
   * @var StreetLines $StreetLines
   * @access public
   */
  public $StreetLines = null;

  /**
   * 
   * @var StreetName $StreetName
   * @access public
   */
  public $StreetName = null;

  /**
   * 
   * @var BuildingName $BuildingName
   * @access public
   */
  public $BuildingName = null;

  /**
   * 
   * @var StreetNumber $StreetNumber
   * @access public
   */
  public $StreetNumber = null;

  /**
   * 
   * @var StreetLines2 $StreetLines2
   * @access public
   */
  public $StreetLines2 = null;

  /**
   * 
   * @var StreetLines3 $StreetLines3
   * @access public
   */
  public $StreetLines3 = null;

  /**
   * 
   * @var CityDistrict $CityDistrict
   * @access public
   */
  public $CityDistrict = null;

  /**
   * 
   * @var City $City
   * @access public
   */
  public $City = null;

  /**
   * 
   * @var StateOrProvinceCode $StateOrProvinceCode
   * @access public
   */
  public $StateOrProvinceCode = null;

  /**
   * 
   * @var PostalCode $PostalCode
   * @access public
   */
  public $PostalCode = null;

  /**
   * 
   * @var CountryName $CountryName
   * @access public
   */
  public $CountryName = null;

  /**
   * 
   * @var CountryCode $CountryCode
   * @access public
   */
  public $CountryCode = null;

  /**
   * 
   * @param StreetLines $StreetLines
   * @param StreetName $StreetName
   * @param BuildingName $BuildingName
   * @param StreetNumber $StreetNumber
   * @param StreetLines2 $StreetLines2
   * @param StreetLines3 $StreetLines3
   * @param CityDistrict $CityDistrict
   * @param City $City
   * @param StateOrProvinceCode $StateOrProvinceCode
   * @param PostalCode $PostalCode
   * @param CountryName $CountryName
   * @param CountryCode $CountryCode
   * @access public
   */
  public function __construct($StreetLines, $StreetName, $BuildingName, $StreetNumber, $StreetLines2, $StreetLines3, $CityDistrict, $City, $StateOrProvinceCode, $PostalCode, $CountryName, $CountryCode)
  {
    $this->StreetLines = $StreetLines;
    $this->StreetName = $StreetName;
    $this->BuildingName = $BuildingName;
    $this->StreetNumber = $StreetNumber;
    $this->StreetLines2 = $StreetLines2;
    $this->StreetLines3 = $StreetLines3;
    $this->CityDistrict = $CityDistrict;
    $this->City = $City;
    $this->StateOrProvinceCode = $StateOrProvinceCode;
    $this->PostalCode = $PostalCode;
    $this->CountryName = $CountryName;
    $this->CountryCode = $CountryCode;
  }

}
