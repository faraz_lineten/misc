<?php

class docTypeRef_WeightType
{

  /**
   * 
   * @var Value $Value
   * @access public
   */
  public $Value = null;

  /**
   * 
   * @param Value $Value
   * @access public
   */
  public function __construct($Value)
  {
    $this->Value = $Value;
  }

}
