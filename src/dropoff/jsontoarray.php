<?php

namespace dropoff;
$list = <<<EOL
{
"success": true,
"timestamp": "2021-04-19T16:57:30Z",
"data": {
"user": {
"first_name": "Kevin",
"last_name": "Lynch",
"id": "9477c9f2314550de42e765c98f286d97"
},
"client": {
"company_name": "Lineten",
"id": "c29116c1d1124ae6d876067f56bfa0e2"
},
"managed_clients": {
"level": 0,
"company_name": "Lineten",
"id": "c29116c1d1124ae6d876067f56bfa0e2",
"children": [
{
"company_name": "Altar'd State- Galleria Ft Lauderdale",
"level": 1,
"id": "611ced0bcb3f4ba63cf6383244f6de2d",
"children": []
},
{
"company_name": "Altar'd State- Merrick Park",
"level": 1,
"id": "1d233e0e280000b57db46db6e9575ff8",
"children": []
},
{
"company_name": "Altar'd Cantera SA",
"level": 1,
"id": "82e0e17f5d574fe4c5a8986b217a50b3",
"children": []
},
{
"company_name": "Lineten Limited",
"level": 1,
"id": "9aea350054946c2b833b0541259ff621",
"children": []
},
{
"company_name": "Altar'd Nashville",
"level": 1,
"id": "25e1f6250bb517aa3c1795e83f7d388a",
"children": []
},
{
"company_name": "Lineten Limited",
"level": 1,
"id": "500a4a3b680a53673544e1f2f7696745",
"children": []
},
{
"company_name": "Altar'd City Centre Houston",
"level": 1,
"id": "fb5bd0f9de7543ad642a8f4046550dd6",
"children": []
},
{
"company_name": "Ace Hardware Denver",
"level": 1,
"id": "ccf55a0958c0d4cdbf5b5c0d32b3c2ce",
"children": []
},
{
"company_name": "Altar'd Alamo Quarry",
"level": 1,
"id": "64bb0f748caa14be7d56828ac1099771",
"children": []
},
{
"company_name": "Lineten Limited",
"level": 1,
"id": "bcb5f3d0da15ea78f33737d53455bb77",
"children": []
},
{
"company_name": "Altar'd Houston Rice",
"level": 1,
"id": "f44e40d168b4a552e022cdfb31e4962e",
"children": []
},
{
"company_name": "Altar'd State - Cherry Creek",
"level": 1,
"id": "1c8c65800f3ff814e055b80ea03e554f",
"children": []
},
{
"company_name": "Altar'd State- International Plaza",
"level": 1,
"id": "7f9adda3c79d9f43feed94b1dc99d5db",
"children": []
},
{
"company_name": "Party City- Las Vegas",
"level": 1,
"id": "81a866c9e61c023709d744b431accb5f",
"children": []
},
{
"company_name": "Altar'd Dallas Allen",
"level": 1,
"id": "7d987b00e72524042d3467d42b6a3ad3",
"children": []
},
{
"company_name": "Altar'd Dallas Galleria",
"level": 1,
"id": "07150f565deec27a938acc7a833d465e",
"children": []
},
{
"company_name": "Altar'd Dallas Frisco",
"level": 1,
"id": "3cd5105047790f8bc3268fb0cb99e347",
"children": []
},
{
"company_name": "Altar'd Domain",
"level": 1,
"id": "5d1d85b2c42d154a07566212a5a0a974",
"children": []
},
{
"company_name": "Farfetch - Miami (via Lineten)",
"level": 1,
"id": "9907e90c4a5f124f0fbb8d64095d8e39",
"children": []
},
{
"company_name": "Pet Planet",
"level": 1,
"id": "511791073d7a66343e771d5a0576e7a8",
"children": []
}
]
}
}
}

EOL;

class Messaging
{
    public static function cliPrint($message)
    {
        echo PHP_EOL;
        echo $message;
        echo PHP_EOL;
    }
}

class Dropoff
{

    public function process()
    {
        global $list;
        Messaging::cliPrint("init");

        $list = json_decode($list);
        print_r($list);
        $file = fopen("dropoff.csv",'w');
        foreach ($list->data->managed_clients->children as $childAccount) {
            Messaging::cliPrint( "{$childAccount->id}    {$childAccount->company_name}");
            fputcsv($file, [$childAccount->company_name, $childAccount->id]);
        }
        fclose($file);
    }
}

(new Dropoff())->process();

?>