var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/booking/status/:id', function(req, res, next) {
  var bookingStatus = {nBookingID:req.params.id, Status:"Waiting"};
  res.send(JSON.stringify(JSON.stringify( bookingStatus)));
});


/* GET users listing. */
router.get('/booking/location/:id', function(req, res, next) {
  var locationStatus = {
    nBookingID:parseInt(req.params.id), 
    Status:"Completed", 
    Locations:[
      {
        sDriverName:"Robert Mason",
      Latitude:51.499049,
      Longitude:-0.085150,
      Timestamp:"2020-09-30T22:19:23.11"
    }
  ]
};
  res.send(JSON.stringify(JSON.stringify( locationStatus)));
});


/* GET users listing. */
router.get('/booking/driverDetail/:id', function(req, res, next) {

  var locationStatus = {
    nBookingID:parseInt(req.params.id),
    Status:"On Route Pick",
    sCallSign:"GT113",
    sDriverName:"Robert Mason",
    sEmail:"emil.rutyna@gmail.com",
    nHomeTelNo:"",
    nMobileNo:"07864858447"
  };

// var locationStatus = {
//   ErrorCode:8, 
//   ErrorMessage:"Driver is not assigned yet.", 
// };

res.send(JSON.stringify(JSON.stringify( locationStatus)));

});

/* GET users listing. */
router.get('/booking/pob/:id', function(req, res, next) {
  let locationStatus = [{
    nBookingCollectionID: parseInt(req.params.id),
    nBookingID: parseInt(req.params.id) ,
    dCollectionDate :"2016-09-26T21:27:48",
    nSequenceNo : 1,
    sContactName : "Purchase Ledger",
    sContactNo : "01236 795131",
    sCollectionNotes : "",
    dPOBDateTime : "2017-03-07T04:35:19.463",
    sPOBSignBy : "113adiddtff",
    nPOBWtTime : null,
    sAddressLine1 : "Ca'd'oro Building",
    sAddressLine2 : "45 Gordon Street",
    sPostCode : "G1 3PE",
    nCountryID: 1,
    sCountryName :"England",
    nCityID : 40,
    sCityName : "Glasgow",
    sCounty : "",
    sCompanyName : "Optical Express Ltd",
    sPOBSignature : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAADCAIAAADZSiLoAAAAI0lEQVQI12N89OgxAwMDAwMDE4TavXsfEwMDw/XrNxcuXAYAdZQJULS1G+IAAAAASUVORK5CYII=",
    sPOBNotes : "ccc",
    sContactEmail : null
}];
  res.send(JSON.stringify(JSON.stringify( locationStatus)));
});

/* GET users listing. */
router.get('/booking/pod/:id', function(req, res, next) {
  let locationStatus = [{
    nBookingCollectionID:parseInt( req.params.id),
    nBookingID :parseInt(req.params.id) ,
    dCollectionDate :"2016-09-26T21:27:48",
    nSequenceNo : 2,
    sContactName : "Purchase Ledger",
    sContactNo : "01236 795131",
    sCollectionNotes : "",
    dPODDateTime : "2017-03-07T04:35:19.463",
    sPODSignedBy : "113adiddtff",
    nPODWtTime : null,
    sAddressLine1 : "Ca'd'oro Building",
    sAddressLine2 : "45 Gordon Street",
    sPostCode : "G1 3PE",
    nCountryID: 1,
    sCountryName :"England",
    nCityID : 40,
    sCityName : "Glasgow",
    sCounty : "",
    sCompanyName : "Optical Express Ltd",
    sPODSignature : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAMAAAADCAIAAADZSiLoAAAAI0lEQVQI12N89OgxAwMDAwMDE4TavXsfEwMDw/XrNxcuXAYAdZQJULS1G+IAAAAASUVORK5CYII=",
    sPODNotes : "ccc",
    sContactEmail : null
}];
  res.send(JSON.stringify(JSON.stringify( locationStatus)));
});

module.exports = router;
