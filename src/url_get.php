<?php
$str = <<<'EOL'
api_key=437b0b70-2245-11e9-a9db-ee18ad1a7911&job_id=20191210-165601-e7f14836db8927b9&external_id=177164&finished=0&status=AT_PICKUP&progress=30&pickup_eta=2019-12-11T07%3A50%3A33%2B00%3A00&delivery_eta=2019-12-11T08%3A12%3A58%2B00%3A00&price_net=0.00&price_gross=0.00&courier_name=Tahir
EOL;
echo PHP_EOL;
echo PHP_EOL;
parse_str($str, $output);
arrayToSting($output, '');

function arrayToSting($output, $parent = '')
{
    foreach ($output as $key => $val) {
        if (is_array($val)) {
            $masterKey = ('' === $parent) ? $key : "{$parent}[{$key}]";
            arrayToSting($val, $masterKey);
        } else {
            $masterKey = ('' === $parent) ? $key : "{$parent}[{$key}]";
            echo "{$masterKey} : {$val}\n";
        }
    }
}