<?php
//error_reporting(E_ALL | E_PARSE);
include "vendor/autoload.php";

use PHPtricks\Orm\Database;

/** @var  $db Database */
printCli("starting");

$host = 'noquds-production-read-replica.noqu.delivery';
$db = 'noquds_production';
$user = 'Faraz';
$pass = 'U3H2TkcYCSr5bdGUqN9Ev8HiQVnnpX';
$charset = 'utf8';
$dsn = "mysql:host=$host;dbname=$db;charset=$charset";

$options = [
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ,
    PDO::ATTR_EMULATE_PREPARES => false,
];

try {
    $pdo = new PDO($dsn, $user, $pass, $options);
} catch (\PDOException $e) {
    die($e->getMessage() . ': ' . (int)$e->getCode());
}


$fp = fopen('report.csv', 'w');
fputcsv($fp, ['NDS Id', 'Client ref', "Time", 'Event', "Who", "IP", "Headers"]);

$time = (new DateTime('2019-10-01'))->getTimestamp();
//find cp jobs
$jobQuery = $pdo->prepare("select `id`, `client_ref` from `job_object` where `client_id`= :client_id and  `created` > :created limit :limit ");
$jobQuery->execute([':client_id' => 71, ':created' => $time, ':limit' => 10000]);
$jobs = $jobQuery->fetchAll();

foreach ($jobs as $job) {
    echo '.';
    //check jdsh
    $jdshQuery = $pdo->prepare(" select `created`, `status` from `job_delivery_status_history` where `job_id`= :job_id and (`status` = 'cancel_requested' or `status`= 'booked' or `status`= 'cancelled' ) order by created ASC ");
    $jdshQuery->execute([':job_id' => $job->id]);
    $jdsHistories = $jdshQuery->fetchAll();
    foreach ($jdsHistories as $jdsHistory) {
        if ('cancel_requested' === $jdsHistory->status) {
            $responseLogs = searchRequestLogs($job->id, $jdsHistory->created, 0);
            if ($responseLogs) {
                foreach ($responseLogs as $responseLog) {
                    $headerString = '';
                    $headers = @unserialize($responseLog->headers);
                    $whoCancelled = "L10";
                    if (isset($headers['HTTP_X_DATADOG_PARENT_ID']) || isset($headers['HTTP_X_DATADOG_SAMPLING_PRIORITY']) || isset($headers['HTTP_X_DATADOG_TRACE_ID'])) {
                        $whoCancelled = "CP";
                        $headerString = 'DATADOG Signature';
                    } elseif (isset($headers['HTTP_USER_AGENT']) && false !== strpos($headers['HTTP_USER_AGENT'][0], 'cURL')) {
                        $whoCancelled = "L10";
                        $headerString = 'L10 HTTP_USER_AGENT';
                    }

                    logToFile($job->id, $job->client_ref, $jdsHistory->created, $jdsHistory->status, $whoCancelled, $responseLog->ipaddress, $headerString);

                }
            } else {
                logToFile($job->id, $job->client_ref, $jdsHistory->created, $jdsHistory->status, "", "", "");
            }
        } else {
            logToFile($job->id, $job->client_ref, $jdsHistory->created, $jdsHistory->status, "", "", "");
        }
    }
}
fclose($fp);

/**
 * @param $jobId
 * @param $created
 * @param int $counter
 * @return array
 */
function searchRequestLogs($jobId, $created, $counter = 0): ?array
{
    echo ',';
    //go deep upto 10sec
    if ($counter == 10) {
        return null;
    }
    global $pdo;

    $logQuery = $pdo->prepare("select `ipaddress`, `headers`, `uri` from  `debug_request_log` where `method` = 'DELETE' and `uri` LIKE :uri  and created =:created");
    $logQuery->execute([':uri' => "http://api.noqu.delivery/v1/job/{$jobId}%", ':created' => $created]);
    $logs = $logQuery->fetchAll();

    if (0 === count($logs)) {
        //search for previous second
        return searchRequestLogs($jobId, $created - 1, $counter + 1);
    } else {
        return $logs;
    }
}

function logToFile($ndsId, $clientRef, $created, $type, $who, $ip, $headers)
{
    global $fp;
    $time = (new \DateTime('@' . $created))->format("Y-m-d H:i:s");
    $array = [
        $ndsId,
        $clientRef,
        $time,
        $type,
        $who,
        $ip,
        $headers,
    ];
    fputcsv($fp, $array);
}

function printCli(string $string)
{
    echo PHP_EOL;
    echo $string;
    echo PHP_EOL;

}

printCli("finish");