<?php
// php index.php 61 gails_putney_11.csv
//-- seymore 52
// -- Chi    53
//-- putney  61


namespace GailsPU;

class Messaging
{
    public static function cliPrint($message)
    {
        echo PHP_EOL;
        echo $message;
    }
}

class DataBase
{
    /** @var DataBase */
    static $instance;
    private $conn;

    private function __construct()
    {
        try {
            $this->conn = new \PDO('mysql:host=noquds-production.cjtv2wuth0pc.eu-west-1.rds.amazonaws.com;dbname=noquds_production', 'noquds_prod_user', 'BXv5uhXIysYiBambEtpMiySFhoR1A');
            $this->conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new DataBase();
        }
        return self::$instance;
    }

    public function getData($query, $parms)
    {
        try {
            $stmt = $this->conn->prepare($query);
            $stmt->execute($parms);

            // set the resulting array to associative
            $stmt->setFetchMode(\PDO::FETCH_ASSOC);
            return $stmt->fetchAll();
        } catch (\PDOException $e) {
            echo $query . "<br>" . $e->getMessage();
        }
    }

    public function update($query, $parms)
    {
        try {
            $stmt = $this->conn->prepare($query);
            $stmt->execute($parms);
            return $stmt->rowCount();
        } catch (PDOException $e) {
            echo $query . "<br>" . $e->getMessage();
        }

    }
}

class GailsPU
{
    function readFile($fileFullPath)
    {
        $f = fopen($fileFullPath, 'r');
        $db = DataBase::getInstance();
        $nextDay = (new \DateTime())->modify('+1day')->format('Y-m-d');
        while ($line = fgetcsv($f)) {
            //check if key is set in file
            if (!empty($line[0]) && !empty($line[1]) && !empty($line[2])) {
                //check what client we are dealing with
                //-- Seymour 52
                // -- Chiswick    53
                //-- Putney  61
                if (false !== (strpos(strtolower($line[0]), 'seymour'))) {
                    $clientId = 52;
                } elseif (false !== (strpos(strtolower($line[0]), 'chiswick'))) {
                    $clientId = 53;
                } elseif (false !== (strpos(strtolower($line[0]), 'putney'))) {
                    $clientId = 61;
                } elseif (false !== (strpos(strtolower($line[0]), 'islington'))) {
                    $clientId = 177;
                } else {
                    throw new \Exception('Unknow Store name: ' . $line[0]);
                }

                $time = new \DateTime("{$nextDay} {$line[2]}", new \DateTimeZone('Europe/London'));
                $time->setTimezone(new \DateTimeZone('UTC'));
                $timeString = $time->format('Y-m-d H:i:s');
                $updated = 0;
                $updated = $db->update("update  job_object set requested_pickup_time= :requested_pickup_time where client_id= :client_id and client_ref= :client_ref limit 1",
                    [
                        ':client_id' => $clientId,
                        ':client_ref' => trim($line[1]),
                        ':requested_pickup_time' => $timeString
                    ]);
                echo "{$clientId}, {$line[1]} , {$line[2]} , {$timeString} :   {$updated}    :{$line[0]} ";
                echo PHP_EOL;
            }
        }

        //process line 0 for titles
        $line = fgetcsv($f);
    }

    public function process($input, $inputCount)
    {
        echo PHP_EOL;

        //validate inputs
        if (!isset($input['1'])) {
            Messaging::cliPrint("Missing args: file name");
            return;
        }

        $fileFullPath = getcwd() . DIRECTORY_SEPARATOR . $input[1];
        if (!file_exists($fileFullPath)) {
            Messaging::cliPrint("file path is wrong: {$fileFullPath}");
            return;
        }

        $this->readFile($fileFullPath);
    }

}

$gailspu = new GailsPU();
$gailspu->process($argv, $argc);