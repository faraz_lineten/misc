<?php

/**
 * php FloomCredentials.php 89 Floom\ Pricing\ UK\ -\ Florist+DP.csv
 *
 *  while true; do clear; php FloomCredentials.php 89 Passage\ November\ Overview\ Updated\ .xlsx ; sleep 10; done
 *
 */

namespace Floom;

class Messaging
{
    public static function cliPrint($message)
    {
        echo PHP_EOL;
        echo $message;
    }
}

class FloomCredentials
{
    private $address1Index;
    private $postcodeIndex;
    private $apiKeyIndex;
    private $bearerTokenIndex;


    private function filterColumns($row)
    {
        foreach ($row as $key => $value) {
            switch ($value) {
                case 'Line 1':
                    $this->address1Index = $key;
                    break;
                case 'Postcode/ Zipcode':
                    $this->postcodeIndex = $key;
                    break;
                case 'API_Key':
                    $this->apiKeyIndex = $key;
                    break;
                case 'Barer Token':
                    $this->bearerTokenIndex = $key;
                    break;
                default:
            }
        }
    }

    private function readFile($fileFullPath, $clientGroupId)
    {
        $f = fopen($fileFullPath, 'r');

        //process line 0 for titles
        $line = fgetcsv($f);
        $this->filterColumns($line);
        if (null !== $this->address1Index && null !== $this->postcodeIndex && null !== $this->apiKeyIndex && null !== $this->bearerTokenIndex) {
            $entries = [];
            while ($line = fgetcsv($f)) {
                //check if key is set in file
                if (isset($line[$this->apiKeyIndex]) && !empty($line[$this->apiKeyIndex])) {
                    if(strpos($line[$this->bearerTokenIndex],"Bearer ")!== false){
                        $line[$this->bearerTokenIndex] = substr($line[$this->bearerTokenIndex],7);
                    }
                    $entries[] = (object)[
                        "Address1" => $line[$this->address1Index],
                        "Postcode" => $line[$this->postcodeIndex],
                        "ClientGroupId" => $clientGroupId,
                        "ApiKey" => trim($line[$this->apiKeyIndex]),
                        "Bearer" => trim($line[$this->bearerTokenIndex]),
                    ];
                }
            }
            file_put_contents('floom_josn.json', json_encode($entries), 0);
        } else {
            Messaging::cliPrint("Columns name not found, 'Line 1', 'Postcode/ Zipcode', ''API_Key' or 'Barer Token' ");
        }
    }

    public function process($input, $inputCount)
    {
        echo PHP_EOL;

        //validate inputs
        if (!isset($input['1']) || !isset($input[2])) {
            Messaging::cliPrint("Missing args ClientGroupId and file path");
            return;
        }
        if (!is_numeric($input[1])) {
            Messaging::cliPrint("ClientGroupId is not a number");
            return;
        }
        $fileFullPath = getcwd() . DIRECTORY_SEPARATOR . $input[2];
        if (!file_exists($fileFullPath)) {
            Messaging::cliPrint("file path is wrong: {$fileFullPath}");
            return;
        }
        $this->readFile($fileFullPath, $input[1]);
    }
}

$instace = new FloomCredentials();
$instace->process($argv, $argc);