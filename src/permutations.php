<?php
$element1 = ['', 'van', 'bike', 'motor', 'walk'];
$element2 = ['', 'std', 'vip'];
$element3 = ['', 'int'];
$element4 = ['', 'dwr_30', 'dwr_1hr', 'dwr_2hr', 'dwr_4hr'];
$element5 = ['', 'eco'];
$element6 = ['', 'hv'];
$element7 = ['', 'scan_barcode'];
$element8 = ['', 'photo_id'];
$element9 = ['', 'signature'];
$element10 = ['', 'c30', 'c25'];


$fopen = fopen('permutations.csv', 'w');
foreach ($element1 as $el1) {
    foreach ($element2 as $el2) {
        foreach ($element3 as $el3) {
            if (!($el2 == 'std' && in_array($el1, ['bike', 'motor', 'walk']))) {
                foreach ($element4 as $el4) {
                    foreach ($element5 as $el5) {
                        foreach ($element6 as $el6) {
                            foreach ($element7 as $el7) {
                                foreach ($element8 as $el8) {
                                    foreach ($element9 as $el9) {
                                        foreach ($element10 as $el10) {

                                            fputcsv($fopen, [$el1, $el2, $el3, $el4, $el5, $el6, $el7, $el8, $el9, $el10]);

                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
fclose($fopen);