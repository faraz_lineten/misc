<?php

/**
 * php FloomCredentials.php 89 Floom\ Pricing\ UK\ -\ Florist+DP.csv
 *
 *  while true; do clear; php FloomCredentials.php 89 Passage\ November\ Overview\ Updated\ .xlsx ; sleep 10; done
 *
 */

namespace callback;

class Messaging
{
    public static function cliPrint($message)
    {
        echo PHP_EOL;
        echo $message;
    }
}

class OnfleetWebhooks
{
    private $address1Index;
    private $postcodeIndex;
    private $apiKeyIndex;
    private $bearerTokenIndex;

    private $apiURL = "https://nds-cb.lineten.tech/api/callback/58f073ab-832b-48b1-bfae-1227405a0880";
    private $apiKey = "";

    public function process($input, $inputCount)
    {
        $this->sendCallback();
    }

    function sendCallback()
    {
        $webhookList = <<<EOF
J775000000058337221
J775000000058362923
J775000000058376614
J775000000058377605
J775000000058434738
J775000000058580799
J775000000058580947
J775000000058581007
J775000000058581115
J775000000058581197
J775000000058581904
J775000000058841891
J775000000058882708
J775000000058884854
J775000000058891062
J775000000058891124
J775000000058891246
J775000000058912163
J775000000058912260
J775000000058919671
J775000000058921269
J775000000058921978
J775000000058922998
J775000000058931370
J775000000058949207
J775000000058949783
J775000000058949885
J775000000058952607
J775000000058976889
J775000000058976930
J775000000058976946
J775000000058977066
J775000000058977770
J775000000058977792
J775000000058977808
J775000000058979703
EOF;


        $jobListArray = explode("\n", $webhookList);

        foreach ($jobListArray as $jobId) {
            $jobId = trim($jobId);
            $headers = ["Content-Type" => "application/json"];
            $payload = $this->getClosedPayload($jobId);
            Messaging::cliPrint($payload);
            $this->curlPost("", $payload, $headers);
        }
    }

    private function getClosedPayload($alJobId): string
    {
        return <<<EOF
{
	"status": "PRICED",
	"job": {
		"id": "{$alJobId}",
		"number": "640048",
		"uuid": "1b3b4a5a-fe22-4d09-a449-6523f3b076d6",
		"vendor": "",
		"pickup_dt": "2020-02-08T10:17:00Z",
		"asap": false
	},
	"account": {
		"number": 826910
	},
	"dates": {
		"event": "2020-02-09T11:44:46Z",
		"sent": "2020-02-09T11:44:47.625Z"
	},
	"journey": [
		{
			"stop": {
				"type": "ADDRESS",
				"wait": 17,
				"address": {
					"formatted_address": "Cafe Route 21 White Conduit Street, London, N1 9HA",
					"address_components": {
						"locality": "London",
						"postal_code": "N1 9HA",
						"country": "GB"
					}
				},
				"geo": {
					"lat": 51.53371,
					"long": -0.10898
				},
				"datetime": "2020-02-08T09:59:40"
			}
		},
		{
			"stop": {
				"type": "ADDRESS",
				"wait": 0,
				"address": {
					"formatted_address": "Founders Forum 2 Derry Street, Northcliffe House, London, W8 5TT",
					"address_components": {
						"locality": "London",
						"postal_code": "W8 5TT",
						"country": "GB"
					}
				},
				"geo": {
					"lat": 51.50137,
					"long": -0.19115
				}
			}
		},
		{
			"stop": {
				"type": "ADDRESS",
				"wait": 0,
				"address": {
					"formatted_address": "Cafe Route 21 White Conduit Street, London, N1 9HA",
					"address_components": {
						"locality": "London",
						"postal_code": "N1 9HA",
						"country": "GB"
					}
				},
				"geo": {
					"lat": 51.53371,
					"long": -0.10898
				},
				"datetime": "2020-02-08T10:55:34"
			}
		}
	],
	"pricing": {
		"net": "34.16",
		"tax": "8.54",
		"total": "42.70",
		"prices": [
			{
				"name": "Journey Mileage",
				"type": "BASE",
				"tax": "01",
				"amount": 16.0,
				"rate": 2.67,
				"unit": "miles",
				"charge": "42.70"
			},
			{
				"name": "Waiting Time",
				"type": "WAIT",
				"actual": 17,
				"amount": 5,
				"rate": 0,
				"unit": "minutes",
				"charge": "0.00"
			}
		]
	},
	"taxes": [
		{
			"name": "01",
			"description": "Standard Rate",
			"rate": 20,
			"unit": "percent",
			"net": "42.70",
			"charge": "8.54"
		}
	]
}
EOF;
    }


    function curlDel($path)
    {
        sleep(2); // onlfeet wait
        $url = $this->apiURL . $path;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_USERPWD, "{$this->apiKey}:");
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return $result;
    }

    function curlGet($path)
    {
        sleep(2); // onlfeet wait
        $url = $this->apiURL . $path;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, "{$this->apiKey}:");

        $result = curl_exec($ch);

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return $result;
    }

    function curlPost($path, $payload, $headers)
    {
        sleep(2); // onlfeet wait
        $url = $this->apiURL . $path;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, "{$this->apiKey}:");

        $result = curl_exec($ch);

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return $result;
    }
}

$instace = new OnfleetWebhooks();
$instace->process($argv, $argc);