<?php

namespace Callback\Filter;
class fileFilter
{
    private $target = "/mnt/c/Users/faraz/Downloads/";
    private $dpKey = "f7cfe547-20e4-449e-8933-33a0f15dfdd3";

    public function readFolders($root)
    {
        try {
            $di = new \RecursiveDirectoryIterator($root);
            $i = 1;
            foreach (new \RecursiveIteratorIterator($di) as $filename => $file) {
                if (is_file($filename) && strpos($filename, $this->dpKey) !== false) {
                    echo $i . " : " . $filename . ' - ' . $file->getSize();
                    echo PHP_EOL;
                    $i++;
                    $this->filterFileContent($filename);
                }
            }
        } catch (\Exception $e) {
            die("can't read folder {$e->getMessage()}");
        }
    }

    public function filterFileContent($filename)
    {
        $content = file_get_contents($filename);
        //echo $content;
        if ($result = preg_match('/\\\"id\\\":\h([0-9]*),/', $content, $matched)) {
            if (count($matched) == 2) {
                $name = basename($filename);
                $to = "{$this->target}{$this->dpKey}/{$matched[1]}/";
                mkdir($to, 0777, true);
                copy($filename, $to. $name);
            }
        }
//        echo PHP_EOL;
//        var_dump($result, $matched, $to);
//        die;

    }
}

$ci = new fileFilter();
$ci->readFolders("/mnt/c/Users/faraz/Downloads/callbacks");
