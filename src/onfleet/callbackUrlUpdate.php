<?php

/**
 * php FloomCredentials.php 89 Floom\ Pricing\ UK\ -\ Florist+DP.csv
 *
 *  while true; do clear; php FloomCredentials.php 89 Passage\ November\ Overview\ Updated\ .xlsx ; sleep 10; done
 *
 */

namespace onfleet;

class Messaging
{
    public static function cliPrint($message)
    {
        echo PHP_EOL;
        echo $message;
    }
}

class OnfleetWebhooks
{
    private $address1Index;
    private $postcodeIndex;
    private $apiKeyIndex;
    private $bearerTokenIndex;

    private $apiURL = "https://onfleet.com/api/v2/";
    private $apiKey = "355df62ece030c046297140b920d6270"; //v1 master
    private $oldURL = "";
    private $newURL = "https://func-nds-cb-staging-cac.azurewebsites.net/api/callback/140bf7b2-6593-432d-9e46-42d7b23dde02";
   // private $newURL = "https://api.onehour-delivery.ca/v1/callback/onfleet/job";


    public function process($input, $inputCount)
    {
        echo PHP_EOL;

        //validate inputs
        if (!isset($input['1'])) {
            Messaging::cliPrint("Missing args option set remove|change|new");
            return;
        }
        switch ($input['1']) {
            case "change";
                $this->changeExisting();
                break;
            case "remove":
                $this->removeAll();
                break;
            case "new":
                $this->createNew();
                break;
            default:
                Messaging::cliPrint("Unknown option selected allowed remove|change|new");

        }

    }

    function createNew()
    {
        $webhookList = <<<EOF
[
	{
		"name": "",
		"trigger": 0,
		"isEnabled": true
	},
	{
		"name": "",
		"trigger": 4,
		"isEnabled": true
	},
	{
		"name": "",
		"trigger": 3,
		"isEnabled": true
	},
	{
		"name": "",
		"trigger": 8,
		"isEnabled": true
	},
	{
		"name": "",
		"trigger": 12,
		"isEnabled": true,
		"threshold": 600
	},
	{
		"name": "",
		"trigger": 12,
		"isEnabled": true,
		"threshold": 500
	},
	{
		"name": "",
		"trigger": 12,
		"isEnabled": true,
		"threshold": 400
	},
	{
		"name": "",
		"trigger": 12,
		"isEnabled": true,
		"threshold": 300
	},
	{
		"name": "",
		"trigger": 12,
		"isEnabled": true,
		"threshold": 200
	},
	{
		"name": "",
		"trigger": 12,
		"isEnabled": true,
		"threshold": 100
	},
	{
		"name": "",
		"trigger": 10,
		"isEnabled": true
	},
	{
		"name": "",
		"trigger": 1,
		"isEnabled": true,
		"threshold": 300
	},
	{
		"name": "",
		"trigger": 1,
		"isEnabled": true,
		"threshold": 200
	},
	{
		"name": "",
		"trigger": 1,
		"isEnabled": true,
		"threshold": 100
	},
	{
		"name": "",
		"trigger": 1,
		"isEnabled": true,
		"threshold": 400
	},
	{
		"name": "",
		"trigger": 1,
		"isEnabled": true,
		"threshold": 500
	},
	{
		"name": "",
		"trigger": 1,
		"isEnabled": true,
		"threshold": 600
	},
	{
		"name": "",
		"trigger": 13,
		"isEnabled": true
	},
	{
		"name": "",
		"trigger": 9,
		"isEnabled": true
	},
	{
		"name": "",
		"trigger": 7,
		"isEnabled": true
	},
	{
		"name": "",
		"trigger": 2,
		"threshold": 100
	}
]
EOF;

        $webHookList = json_decode($webhookList);
        foreach ($webHookList as $webhook) {
            $headers = ["Content-Type" => "application/json"];
            $payload = [
                "url" => "{$this->newURL}",
                "trigger" => $webhook->trigger,
            ];
            if (isset($webhook->threshold)) {
                $payload['threshold'] = $webhook->threshold;
            }
            $payload = json_encode($payload);
            Messaging::cliPrint($payload);
            $this->curlPost("webhooks", $payload, $headers);
        }


    }

    function removeAll()
    {
        $result = $this->curlGet("webhooks");

        if ($result) {
            $webHookList = json_decode($result);
            foreach ($webHookList as $webhook) {
                $this->curlDel("webhooks/{$webhook->id}");
                Messaging::cliPrint("Old trigger deleted {$webhook->trigger}");
            }
        }
    }

    function changeExisting()
    {
        $result = $this->curlGet("webhooks");

        if ($result) {
            $webHookList = json_decode($result);
            foreach ($webHookList as $webhook) {

                if ($webhook->url === $this->oldURL) {
                    Messaging::cliPrint("Old trigger {$webhook->trigger}");
                    // add new webhook with same config but new URL
                    $headers = ["Content-Type" => "application/json"];
                    $payload = [
                        "url" => "{$this->newURL}",
                        "trigger" => $webhook->trigger
                    ];
                    if (isset($webhook->threshold)) {
                        $payload['threshold'] = $webhook->threshold;
                    }
                    $payload = json_encode($payload);
                    $postResponse = $this->curlPost("webhooks", $payload, $headers);
                    if ($postResponse) {
                        $postResponseObj = json_decode($postResponse);
                        if (isset($postResponseObj->id)) {
                            Messaging::cliPrint("New trigger registered {$webhook->trigger}");
                            $this->curlDel("webhooks/{$webhook->id}");
                            Messaging::cliPrint("Old trigger deleted {$webhook->trigger}");
                        }
                    }
                }
            }

        }

    }


    function curlDel($path)
    {
        sleep(2); // onlfeet wait
        $url = $this->apiURL . $path;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_USERPWD, "{$this->apiKey}:");
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return $result;
    }

    function curlGet($path)
    {
        sleep(2); // onlfeet wait
        $url = $this->apiURL . $path;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, "{$this->apiKey}:");

        $result = curl_exec($ch);

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return $result;
    }

    function curlPost($path, $payload, $headers)
    {
        sleep(2); // onlfeet wait
        $url = $this->apiURL . $path;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERPWD, "{$this->apiKey}:");

        $result = curl_exec($ch);

        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return $result;
    }
}

$instace = new OnfleetWebhooks();
$instace->process($argv, $argc);