<?php

class Test {

    function __construct(){
        $this->largeObject = new \stdClass();
        $this->largeObject->menu = new \stdClass();
        $this->largeObject->menu = [
            (object)['prodId' => 1, 'name' => 'burger'],
        ];
    }
    function getProd(){
        $this->setProds();
        return $this->largeObject->menu[0];
    }

    function setProds(){
        $this->largeObject->menu[0]->name = 'burger';
    }

}

$test = new Test();
$result[] = $test->getProd();
$result[0]->name = 'burger1';
$result[] = $test->getProd();
$result[1]->name = 'burger2';

print_r($result);

print_r($test->largeObject);
?>